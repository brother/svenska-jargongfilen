# Svenska jargongfilen

Filen senast ändrad: <2023-04-29 22:34:57>

## Inledning

*Svenska jargongfilen*, i vissa versioner kallad *Svenska hackademins
ordlista*, är en ordlista över svensk hackerslang och ett mindre
uppslagsverk för den svenska hackerkulturen.

Det är tänkt att filen ska vara ett levande dokument och dokumentera
framför allt modernt språkbruk men även viss historisk användning kan
få plats, framför allt för att göra äldre texter förståeliga.

Filen är skapad efter modell av den amerikanska [The Jargon
File](http://www.catb.org/jargon/html/) ([historiska
versioner](https://jargon-file.org/archive/)), i pappersversion känd
som *The New Hacker's Dictionary* eller *TNHD*.

*Svenska jargongfilen* är sammanställd av frivilliga från svensk
hackerkultur.

Ordlistan får spridas och ändras utan begränsningar.

## Tillgängliga versioner

Det finns åtminstone fyra versioner av *Svenska jargongfilen* i omvänd
kronologisk ordning:

- Denna version är sedan 2021 skriven i Markdown på
  [Codeberg](https://codeberg.org/mchack/svenska-jargongfilen).

- [MC:s Texinfo-version](archive/texinfo/hackerswe.texi) ([renderad
  HTML på hack.org](https://hack.org/mc/writings/hackerswe/hackerswe.html))
  senast rörd 2003. Sjätte upplagan.

- SGML-version i Linuxdoc DTD från 1998. Finns nog någonstans i
  [Stackens](https://stacken.kth.se/) CVS. Femte upplagan.

- Staffan Vilcans' variant [Svenska Hackedemiens ordlista, fjärde
  upplagan V4.0C 960531](archive/shol4.0c.html) från 1996. [Renderad
  HTML på hack.org](https://hack.org/mc/writings/shol4.0c.html)

- Ahrvid Engholms version. Tredje upplagan.

- Lars Aronssons version. Andra upplagan.

- NADJA-versionen med titeln [Hacker-uttryck på KTH (NADA och
  Elektro), LiTH samt Uppsala](archive/hackerswe.txt)
  utan datum men troligen 80-tal. Första upplagan.

## Historia

Den svenska jargongfilen går tillbaka till tidigt 80-tal, då
Lars-Henrik "Krsna" Eriksson, Per "TMP" Lindberg (The Mad Programmer)
och Per "pd" Danielsson, satte ihop den första svenska jargongfilen.
Se [Stackens](#stackens) tidning [StackPointer 2, 1981 sidan
49](http://elvira.stacken.kth.se/~mz/sp/stackpointer-1981-2.pdf) för
vad som antagligen är första publiceringen på papper. Denna fil hölls
under många år uppdaterad på maskinerna [AIDA](#aida) vid Uppsala
universitet, på [NADJA](#nadja) på KTH i Stockholm och på
[LINUS](#linus) vid Linköpings universitet.

Patric Hadenius och Ulf Nyrén omarbetade 1984 den ursprungliga
amerikanska *Jargon file*, då redan utgiven på papper 1983 som Guy L.
Steeles *The Hacker's Dictionary*, till svenska förhållanden. De
svenska redaktörerna verkar ha varit mer eller mindre ovetande om den
redan då existerande svenska jargongfilen. En del uttryck i
omarbetningen är överlappande men annars är det rena översättningar
från [amrisk](#amrisk) hackiska. En del av dessa stred då mot
konsensus hos övrig svensk hackerdom. Hadenius' och Nyréns version
publicerades med titeln *Uppslagsbok för datafreakar* på Tidens förlag
1985.

Så småningom kom de tre ursprungliga svenska jargongfilerna att glida
ifrån varandra och bara linköpingsversionen hölls fortfarande
uppdaterad av medlemmar i datorföreningen [Lysator](#lysator),
framförallt av Lars Aronsson.

Någon gång under 1990-talet tog sig Ahrvid Engholm an att redigera
filen och ge ut den på nytt. Han flätade ihop de versioner som gled
runt på nätet till en. Tillsammans med [SAFA:s](#safa)
tisdagslunchare, som skämtsamt kallade Svenska Hackademin, kom man på
det fyndiga namnet *Svenska hackademins ordlista*.

Staffan "Ralph 124C41+" Vilcans lade 1994 ihop Ahrvids version med en
hel del ord och uttryck från BBS-, demo- och cracking-scenerna och
publicerade en egen version.

Ovetande om Vilcans planer hade Aronsson 1994 satt upp en brevlista på
Lysator för att diskutera de vidare uppdateringarna av den version av
ordlistan som underhölls där. Här blev det lite bråk om vem som
egentligen hade rätt att redigera ordlistan.

Debaclet utmynnade efter en tid till att filen skulle underhållas som
källkod i ett programmeringsprojekt med versionshantering. Ett
CVS-projekt på Stacken sattes upp för ändamålet.

Ett omfattande redigerings- och märkningsarbete tog vid. Michael "MC"
Cardell Widerkrantz skrev igenom hela listan, utökade många
definitioner och samlade in och skrev in många nya definitioner. Många
nya bidrag och kommentarer kom från bland andra Christer Boräng,
Chalmers Datorförening, Per "ceder" Cederqvist, Calle Dybedahl och
Hans Persson, Lysator, Nils Olof "noppe" Paulsson, Ctrl-C, och Per
Starbäck.

MC märkte också hela ordlistan enligt SGML DTD:n linuxdoc. Filen
konverterades till HTML för publicering och fanns från 1998 och framåt
från Lysator och [Temple of the Moby
Hack](https://hack.org/mc/writings/hackerswe/).

Något senare automatkonverterade MC hela filen från SGML till det
mycket mer lättredigerade TeXinfo-formatet, skrev igenom hela listan
på nytt och samlade in många fler ord och kommentarer och publicerade
på nytt. Den versionen rördes senast 2003.

2021 skrev MC om rubbet i Markdown, lade till ytterligare ord han hade
liggande och redigerade för mer enhetlighet. Filen publicerades först
på Gitlab och sedan på
[Codeberg](https://codeberg.org/mchack/svenska-jargongfilen) för att
på något vis komma tillbaka till gemensamt redigeringsarbete och i
hopp om att se förnyat intresse.

## Speciella tecken

Vissa teckenkombinationer har speciell betydelse framförallt i
textbaserad elektronisk kommunikation, där man saknar kroppspråkets
alla möjligheter att uttrycka känslor. Ibland kallas dessa symboler
emotikoner. 

Till dessa teckenkombinationer hör det som svensk hackiska kallar
[gladman](#gladman), på engelska "smileys". Dessa gladmän kan
kombineras av alla möjliga tecken, men den [kanoniske](#kanonisk)
gladmannen, och den kombination som gett namn åt dem alla, ser ut så
här: `:-)` och antyder att det författaren just skrivit inte skall tas
på fullt allvar eller att [hän](#hän) ler under det att hän skriver.

Om du inte förstår varför ett kolon följt av ett minustecken och en
parentes kan vara leende, prova att luta huvudet åt vänster och titta
på teckenkombinationen igen.

Andra gladmän (ja, de kallas gladmän även för andra känslor):

`:-(` Ledsamt, till exempel "Mitt moderkort har brunnit".

`:-I` Det är allvar (även om det låter som ett skämt)

`;-)` Ta det inte så allvarligt. Glimten i ögat.

`!-(` Sent på natten.

`@=` Krig! Eller meddelanden om kärnvapen.

`<3` Kärlek.

`=>B^>=` författaren är en djävulskt (horn i pannan) hånflinande
punkare med mohikan, pilotglasögon, stor näsa och bockskägg.

Samtliga förekommer också utan näsa: `:)`, `:(` et cetera.

Andra, besläktade, sätt att markera till exempel ironi är
ironiparenteserna:

```
(. Denna sats är sann. .)
```

som ursprungligen troligen kommer från konferenssystemet [KOM](#kom).

I Usenet News och en del webforum är det i stället ganska vanligt att
använda HTML-liknande taggar som:

```
<flame>Klingon!</flame>

```

eller populära textmärkningsspråk som LaTeX eller Scribe som i:

```
{\flame Luser!}

```

Ett sätt att markera handling i text är att använda "*", till exempel:

```
*fniss*
```

## -1

[Universal default i TOPS-20] Exempel: "- Var ska vi äta i dag då? -
Minus ett!" och så går de iväg till samma ställe som vanligt.

-1-svaret på svaret -1 betyder "Nej, inte nu igen!". 

Vid ett par sällsynta tillfällen då denna terminologi drivits till sin
spets har det hänt att följande dialog utväxlats: 

Hacker A: "Minus ett?"  
Hacker B: "Minus ett?"  
Hacker C: "Minus ett!"  

Översatt: "Var ska vi äta idag, då?" "Restaurang China Garden?" "Nej,
inte nu igen!".

## 17

Metauttryck. Betecknar Det Minst Slumpmässigt Valda Talet. Används då
man vill beteckna ett tal, vilket som helst. Dess ursprung är höljt i
dunkel. Används med förkärlek av övningsassistenter: "Tag ett tal, låt
oss säga 17, och..."

## 42

1. Det Slutgiltiga Svaret på frågan om Livet, Universum, och Allting.

2. Svaret på frågan "Vad är 6*9?", vilket inses lätt om man råkar vara
utrustad med 13 fingrar.

Se Douglas Adams *Liftarens guide till galaxen*, originaltitel *The
Hitch-Hiker's Guide to the Galaxy*, ofta kallad *THHGTTG*.

## 150-ohmare

Långsmal kaka fylld med arrakssmakande kaksmulor och med yta av
marsipan och choklad. Eftersom den har färgmarkeringen brun-grön-brun,
kallas den förstås efter det motstånd den liknar. Kakan kan med fördel
värmas i mikrovågsugn. I det senare fallet kallas den mikrad
150-ohmare.

## 666

Vilddjurets tal (se *Bibeln*, Uppenbarelseboken 13:18). Farliga saker.
Sägs ha förbindelse med Djävulen och andra demoniska väsen. Ett helt
vanligt tal. Mycket vanligt, speciellt i datorsystem som har med
kreditkort och betalningssystem att göra.

## 1337

[Från leetspeak]. Leet, elite, någon som är bra på något. Ofta
använt ironiskt som i "I'M IN! I AM 1337 H4XXOR!" när man lyckades
använda sin nyregistrerade Yubikey för första gången.

## 4711

Används ibland i stället för 17, vid tillfällen då man vill beteckna
ett lite större tal. Kommer troligen från namnet på äkta
Eau-de-cologne, "No. 4711".

## 11147

4711 oktalt. Används ibland även decimalt för ett slumpmässigt valt
exempel av ett något högre tal än 4711.

## achtungtejp

[CD, basvrak] Svart- och gulrandig varningstejp. Vida uppskattad för
styrka, elasticitet, vidhäftningsförmåga och piffigt utseende.

Bland mer synthiga hackare var det en period populärt att låta
achtungtejp pryda jackor och rockar på axelklaffar eller som
armbindel.

En särskilt eftertraktad variant av denna modeattiralj har istället
texten "CYBER" i svart på gult.

## ACK

Meddelandet mottaget och förstått. Skrivs oftast med stora bokstäver
för att undvika förvirring med svenskans uttryck "Ack!".

## ADB

Nedsättande ord om databehandling, i synnerhet men ej endast
administrativ sådan.

## AFD

April Fools' Day. 1:a april. En av de få högtider hackers firar. Se
också [julfest](#julfest).

## AFDD

April Fools' Day Dinner. Tradition vid [Lysator](#lysator) att äta
exotisk mat, gärna kinesiskt, på [AFD](#afd). Knyter an till
MIT-hackarnas tradition som startades av Bill Gosper.

## Agnes

Svensk benämning på "Angus" (riktigt namn), ett chip i datorn
Commodore Amiga. Senare varienter kallas i direkt översättning för
feta Agnes och superfeta Agnes.

## Allan

Vanlig skräpvariabel eller filnamn på Stacken. Kombineras ofta med
kaka som i "Allan tar kakan" eller "Allan tar kakan i 4800 baud!"
eller "Allans kakburk" som också var namnet på ett KOMpatibelt
BBS-system.

## allockera

Att reservera någonting, till exempel ett bord på en restaurang
Just detta kallas också ibland för att utföra `AllocTable()`.

## AMIS

En [EMACS](#emacs)-liknande skärmeditor som ursprungligen skrevs i
Pascal av ett antal hackare på [Stacken](#stacken) och minst en från
[Lysator](#lysator) för att de inte stod ut med att använda editorn
VIDED under Tops-10. AMIS står bland annat för "Anti-MISär".

AMIS skrevs från början under Tops-10 men portades senare till
åtminstone TOPS-20, RSTS/E och VMS.

Se [Sagan om AMIS](https://hack.org/mc/texts/amis.txt).

[AMIS källkod](https://github.com/PDP-10/amis/tree/master/files).

## amrisk

Att ha att göra med USA. Se också [usaiansk](#usaiansk).

## anka

Att söka efter något på nätet, typisk genom att använda sökmaskinen
[Duckduckgo](https://duckduckgo.com/). "Jag ankade efter Google idag."

Även "ankning". Exempel från chat:

```
18:11 <hacker1> Men nu mot Liberöl!
18:12 <hacker2> Liberöl? *ankning intensifieras* Aha! Finns
                Frihetsfronten fortfarande?
```

## användare

Nedsättande om en programmerare som tror på allt du säger till honom.
Någon som ställer frågor i stället för att läsa innantill i
manualerna.

## Any-tangenten

Annat namn på den stora av/på tangenten. Det är den man skall trycka
på då maskinen säger "Press any key". Se även [bita av
sladden](#bita-av-sladden) och [hård boot](#hård-boot).

## armbågstestet

Se [Mathiasmetoden](#mathiasmetoden).

## attans otur

Att fint beklaga (någon annans) misstag, baserat på ren klumpighet,
eller okunnighet. Exempel: "Attans otur att det inte finns ett
systemprogram som sätter privilegiebiten."

## Autokvadd

Ironiskt menad benämning på AutoCAD. Även känt som AutoKladd.

## Autoträsk

Skämtsam benämning på AutoDesk. Se även [Träsket](#träsket).

## automagiskt

Automatiskt, men så pass invecklat att jag inte har tid eller lust att
förklara det just nu. Se [magi](#magi).

## avlusa

Att ta bort fel (en [bugg](#bugg)) i program. Från engelskans "debug".

## bagataljer

En blandning av detaljer och bagateller, ofta i ironisk mening, ofta
om en [bug](#bug): "Om man försöker måttsätta på en gammal ritning så
flimrar skärmen till och maskinen börjar räkna minne." "Äsch!
Bagataljer!"

## bagatesser

[CD] En blandning av bagateller och petitesser.

## Babbe

Förkortad benämning på restaurang Babylon i närheten av KTH. Vattenhål
för Stockholms modemare sedan urminnes tider.

## bakben

Skämtsam översättning av engelskans "backbone", ryggraden i ett
större datornät.

## bakslask

Tecknet **\\**.

## Ballmer peak

Nivån av alkohol i blodet när programmeringsförmågan är som högst.
Från [XKCD 323](https://xkcd.com/323/).

## bar

Syntaktisk metavariabel. Se också [foo](#foo). Har inget med drinkar
eller varm korv att göra.

## barfa

Från engelskan. Kräkas på, dö av et cetera: "Burken barfar på den här
ritningen" betyder att programmet inte klarar att ladda den
ritningen. Även i formen "Barfa ur".

## BARFF

[Ur ett inlägg av Thomas Padron-McCarthy i Flippade förbindelser i
Lysators KOM] Beväpnad AttackRobot Från FOA. Mytologisk robot som
Försvarets forskningsanstalt FOA i Linköping framställt. Känd för sina
pacifistiska egenskaper i det att den, som alla goda robotar enligt
Asimovs robotlagar, optimerade bort alla rutiner som handlade om att
skada människor.

Används i dagligt tal som den arketypiska felaktiga användningen av
teknik som till slut blir god genom inneboende egenskaper, till
exempel Asimovs robotlagar. "-Vad tror du om Hololens? - Det är en nog
en BARFF."

## bas

En BBS, bulletin board system, som kan vara ett
[menyklägg](#menyklägg) eller ett [KOM](#kom).

## basen

1. Den BBS du oftast hänger på.

2. Datateknologsektionen vid Chalmers tekniska högskola hade en lokal
   vars officiella namn var dBase III men som populärt kallades
   "basen".

## basvrak

1. En [IRC](#irc)-kanal för mest gamla chalmerister och hangarounds.

2. En person anknuten till Chalmers datateknologsektion som brukade
   hänga på [Basen](#basen) betydelse 2 lite för mycket och/eller
   ovanstående IRC-kanal.

## banan

1. Ytterligare en metavariabel, som [foo](#foo), men mindre använt.
Används också som ett godtyckligt lösenord, se [gazonk](#gazonk).

2. Banankontakt.

## bibbla

Filbibliotek, directory, filkatalog. Nästan aldrig om ett bibliotek
som innehåller böcker. Se även [dirr](#dirr).

## bibel

Uttrycket tolkas olika berönde på i vilket sammanhang det används.
Inom C-programering avses oftast [K&R](#kr). Annars är bibeln en
vanlig benämning på manualer av olika slag, vanligen för programmering
eller operativsystem.

Fler exempel på biblar är [Drakboken](#drakboken), [SICP](#sicp),
[GEB](#geb) med flera.

## binär

[Engelskans "binary program"] En binär är ett kompilerat (körbart)
program. Motsats till källkod.

## bit

Ibland "bitt". Binär siffra. Används för att uttrycka tal i basen 2.

## bitröta

Vanligt att skylla på när utdata från en stor teknisk beräkning inte
blev vad man väntade. Att slumpmässigt valda bitar i datan får för sig
att ändra tillstånd. Se även [programröta](#programröta).

## bita av sladden

Slå av datorn och slå på den igen. Liktydigt med att behöva göra en
[hård boot](#hård-boot).

## bithink

Ibland bitthink.

1. En tänkt behållare för alla förlorade data, av engelskans "bit
bucket".

2. Skämtsamt namn på en null device: Unix:s `/dev/null`, VMS:s `NL:`,
TOPS-20:s `NUL:`.

## bitt

Svenskt namn på bit.

## bittar

1. Plural av [bit](#bit).

2. (Från TOPS-20) Privilegium; den som är privilegierad har bittar.
   Även på system som inte har sådana bittar, som traditionell Unix,
   så har man ändå bittar om man kan bli root.

## blargh

Uttryck för ett [obra](#obra) tillstånd vad det gäller en person.
Motsatsen är [ping](#ping).

## blowrak

En variant av [svorak](#svorak) för programmerare, ursprungligen
designad av Anders Waldenborg.

## bojsänke

Vanligen oanvändbar, inte särskilt intressant, hårdvara, till exempel
en [vanilj](#vanilj)-PC.

## bot

\/bått\/

Ett vanligen hjälpsamt program som hänger på en [IRC](#irc)-kanal till
exempel för att logga vad som sägs, göra det sökbart eller tala om
ifall något händer. Exempelvis: 

```
<gitbot> mc pushade just till branchen foo i bar
```

Används även för liknande hjälpsamma program i andra chatsystem än
IRC.

## box

1. Dialogbox. Se [oxe](oxe) och [dialogoxe](#dialogoxe).

2. Elektronisk enhet för att ringa gratis. Finns i fler olika färger
berönde på metod. Se även [boxa](#boxa).

## boxa

Att manipulera telefonlinjer på olika sätt för att ringa gratis.

## brygga

Vanligt förekommande översättning av engelskans "bridge", en
förbindelselänk på låg nivå i ett datornät.

## bräda

1. Kretskort. Se också [morsabräda](#morsabräda).

2. Kanban-tavla eller liknande statustavlor i verktyg för
   programmeringsprojekt.

## *BSD

Någon [smak](#smak) av [*nix](#nix)-varianterna FreeBSD, OpenBSD,
NetBSD, Dragonfly BSD med flera.

## bug

Litet programfel. På svenska ibland kallat [bugg](#bugg). Se också
[pug](#pug).

## bugaloo

Interjektion hörd när maskinen dyker (se [dyka](#dyka)), ofta strax
efterföljd av "dansa rok-n-råla".

## bugg

Av engelskans "bug". Innebär ett (oftast litet) fel i ett program.
Kallas även lus. Se [avlusa](#avlusa).

## burk

Dator, terminaler och maskiner i allmänhet kan benämnas burk, data,
hink, kärra (antagligen flygtekniskt ursprung) eller pyts (antagligen
radiotekniskt ursprung).

## bygga

Konstruera, sammanställa, laga mat. "Var har du varit? - Jag har byggt
labbrapport i sju timmar nu. - Då är du nog hungrig. Skall vi bygga
oss något att äta?"

## böp

[CD, basvrak]

Allmängiltig, omnibetydande lingvistisk variabel. Uttalas med ett
långt, slutet ö, ungefär som i ordet "knô". Ordet används som
placeholder för namn på ting.

Skillnaden mellan en böp och en [mojäng](#mojäng) är, att en mojäng är
relativt begränsad i omfång och kan manipuleras på ett eller annat vis
i betydligt större utsträckning än en böp.

Vanligt är också verbet böpa, som kan användas i stället för nära nog
vilket annat verb som helst. Sålunda kan frasen "Emil skall böpa sin
böp" betyda till exempel "Emil skall laga sin cykel", "Emil skall koka
sin pasta" eller "Emil skall ha sex med sin flickvän" (den sista av
dessa tre betydelser är dock relativt ovanlig). Vad orden betyder när
de används, utläses oftast av sammanhanget, men för att förtydliga kan
ordet kombineras med prefix eller suffix, såsom plastböp eller
böphållare. Som den seriellt bevandrade noterar, finns i bruket av böp
och böpa en viss likhet med orden smurf och smurfa. Basvrakslingon
berikades med detta ord någon gång under första halvåret 1994. Ordet
finns även i specialfallet surböp, vilket betyder "allmänt grinig och
avogt inställd person", eller kortare "institutionskanslist" eller
"tentavakt".
    
## C=

En fyndig förkortning av Commodore då deras logo ser ut ungefär på det
viset. Kallas även [kycklinghuvud](#kycklinghuvud).

## C3

Chaos Communication Congress, en tysk hackerkonferens som hålls i
mellandagarna varje år sedan 1984 organiserad av [Chaos Computer
Club](https://www.ccc.de/). Varje års konferens heter vanligtvis
ordningsnummer följt av C3, exempelvis 28C3.

## cabriolet

Att köra nercabbat eller att köra datorn som cabriolet är då man av
överhettningsskäl eller för fixande av hårdvaran kör datorn utan skal
eller med öppna skåpdörrar.

## CADa

Starta om sin dator medelst Ctrl, Alt & Delete. Se [toota](#toota).

## cancerknapp

Knapp för de-gauss som finns på vissa äldre monitorer. Härstammar från
okunnig person på Danderyds Gymnasium som sade att "Den ska man trycka
på, annars får man cancer.".

## CC

1. Coca-Cola. Se [Hackerwasser](#hackerwasser).

2. C-kompilator, efter C-kompilatorn under Unix.

## CCCC

Colossal Cave Computing Center, en datorhall som drevs i hobbysyfte av
ett gäng hackers i anknytning till Stacken, framförallt Peter
Löthberg. Innehöll ett flertal veterandatorer, bland annat KI-10:an
[KICKI](#kicki). Namnet kommer ur textäventyrsspelet Adventure (för
övrigt det första i sitt slag), vars scenario är i en "Colossal Cave".
CCCC var en djupt belägen källarlokal.

## CD

1. Förkortning för datortillverkaren Control Data.

2. Förkortning för Chalmers Datorförening i Göteborg.

3. Charles Dickens. Krog på Söder där modemare hängt runt.

## CDR

[Från Lisp] \/Kudder\/

1. Att ta resten av något.

2. Vanligt utrop då någon vill byta samtalsämne.

## Club-Mate

Tyskt [hackerwasser](#hackerwasser). Koffeinrik läskedryck med säregen
smak. Man vänjer sig! Se även [tschunk](#tschunk).

## COBOL-fingrar

Fingrar som består av små korta stumpar, kapade ungefär vid första
leden. Sjukdom som kommer av att programmera i COBOL, som är ett
programspråk där texten blir örhört voluminös. COBOL-fingrar beror på
att man nött ned fingrarna på tangentbordet.


## coder

Uttryck från demovärlden. En person som programmerar assembler på låg
nivå. Nästan alltid är det frågan om [demo](#demo) eller spel.

## coffee wood

Väldigt direkt översättning till engelska av det svenska uttrycket
"kaffeved". "Det susade nästan hela sträckan men sen blev det bara
coffee wood av det!"

## commonist

Användare på KOM-systemet Common, ursprungligen körande Ulf Hedlunds
TCL. Har ingenting med politik att göra.

## consa

[Från LISP] Att sätta ihop något. "Om vi consar de här rutinerna bör
vi få ett fungerande program".

## computter

Dator. Försvenskning av engelskans "Computer". Se [burk](#burk). "Den
här computtern dyker snart!"

## copy party

En tillställning där [coders](#coder), [swappers](#swapper),
[traders](#trader) och så vidare träffas för att byta [warez](#warez),
titta på [demos](#demo) och dylikt.

## corea

\/Kåra\/

1. Maskinen [dyker](#dyka), slutar fungera, lägger en core,
dör, mular et cetera. Uttrycket kommer av att unixbaserade datorer
skriver ner minnesinnehållet i en fil vid namn "core" eller möjligen
"programnamn.core" då ett program kraschar.

2. [EMT] Säcka ihop.

## cracka

Att forcera ett inloggningssystem eller kopieringsskydd. Se
[cracker](#cracker).

## cracker

Även [knäckare](#knäckare).

1. En person som ägnar sig åt att plocka bort kopieringsskydd från
kommersiella produkter.

2. Person som ägnar sig åt datorintrång.

## CUSP

Från engelskans korform för "Commonly Used System Program". Ett
väldigt bra program; en favorit. Ett program som används av alla
hackers. Emacs är en CUSP.

## cuspig

Något som är otroligt bra. Se [CUSP](#cusp).

## Danny Kaye

Omskrivning av "Hit any key to continue..." till "Hit Danny Kaye to
continue...".

## data

1. Synonym för dator. I Hannes Alvens *Sagan om den stora
datamaskinen* dras detta till sin spets, när ordet böjs data, datan,
dator, datorna.

2. Att använda en dator, ofta i avancerad kontext, som till exempel
   syssla med programmering eller hårdvarukonstruktion. "- Vad gör
   du? - Sitter å datar. Bygger ett nytt programmeringsspråk på mitt
   eget OS på en FPGA."

## demo

Ett program vars enda syfte är att i grafik, musik och programmering
imponera så mycket som möjligt. Görs vanligen av en grupp under ett
gemensamt namn. Viktigast är [codern](#coder) som sköter
programmeringen. Ett subset av demon är [intron](#intro).

## Det bjuder vi på

Mindre fel som man får stå ut med, som man inte orkar rätta.

## Det mörka stället

1. Terminalfönster. Naturligtvis med grön text på svart botten.

2. Alternativ benämning på MS-DOS som är väldigt mörkt jämfört med
till exempel Windows. Kommer från en barnhistoria i *PC Magazine*.

## DHDBD

[Lysators KOM: I\]M] Du har det bra du.

## dialogoxe

Dialogbox. Förkortas oftast [oxe](#oxe) : "Skulle man inte kunna göra
en annorlunda oxe till måttsättningen?" "Orkar inte."

## diddla

Pillra lite planlöst med något, ofta med oväntat resultat. Se även
[fippla](#fippla).

## dig

1. Digitizer. Även kallat "digitt-ser".

2. Ett praktiskt program för att slå upp saker i DNS.

## dirr

Filkatalog. Kort version av engelskans "directory".

## dok

Dokumentation.

## doof

Skräp, fel, dumt, skit: "Provade du KT?" "Ja men det blev bara doof".

## doofa

Verbformen av doof, men oftast i betydelsen att klanta till det: "Oj,
nu doofade jag bort såsen till KT" Se även [supa bort](#supa-bort).

## Drakboken

Aho, Sethi, Ullman: *Compilers: Principles, Techniques, and Tools*, en
klassisk lärobok i kompilatorutveckling.

## dret

[Från Värmländskan?] Skräp. "Jag provade KT men det blev bara dret".
Se även [coffee wood](#coffee-wood).

## drivare

Drivrutin.

## drivis

Drivrutin.

## dubbelfnutt

Tecknet **"**. Men se också [fnutt](#fnutt).

## dumkraft

Förklaringen till varför saker och ting alltid går fel. Kan också
användas om personer som är uttryck för detta. Dumkraft är
naturkraften som styr världen.

Ibland "dummkraft".

## dumpa

1. En process dyker, och möjligen [dumpar den kor](#dumpa-kor).

2. Tömma tarmen.

## dumpa kor

Alternativ omskrivning av "dumpa core" ([corea](#corea), troligen med
referens till teckningar av den amriske serietecknaren Larson.

## dunderklump

Benämning på en Teletype ASR33, urtypen för en pappersterminal. Den
kännetecknas av sin stora vikt och höga ljudnivå. Har gett upphov till
beteckningen TTY som avser terminal i många OS.

## dux

[Stacken] Hackisk förkortning av Digital Unix. Tidigare känt som OSF/1
men numera återigen omdöpt till Tru64.

## DVBF

[Lysators KOM] Det var bättre förr.

## dyka

Totalt systemsammanbrott i en dator. Kännetecknas av att systemet inte
längre gör någonting. Innebär för det mesta att mycket arbete går
förlorat. "Datorn dök i går kväll". Kallas även för att
[krascha](#krascha), [störta](#störta), [gå ner](#gå-ner). När datorn
är riktigt seg kraxar olyckskorparna "Nu är den på väg ner.", "Nu
dyker den snart.".

## dö

Programmet eller maskinen stannar.

## död

Om utrustning, ej fungerande.

## dörrstopp

Se [bojsänke](#bojsänke).

## ECS

Förkortning för "Enhanced Chip Set", ibland även uttolkat/felstavat
som "Enchanted Chip Set" (Se [magi](#magi)). En samling customchips
för Amiga.

## Elake förhörsledaren

Fantasifull hacker som provar en användares program med avsikt att
hitta fel och onödiga begränsningar. Till exempel program som inte
klarar ppn. Engelskans "devils advocate".

## Emacs

En familj av texteditorer med ursprung i EMACS från MIT:s AI-lab.
EMACS var skriven i scriptspråket i en annan editor, TECO, vilket
också innebar att man kunde utöka den med mer TECO.

Nu för tiden syftar man oftast på [GNU
Emacs](https://www.gnu.org/software/emacs/) som också har ett
kraftfullt scriptspråk, Emacs Lisp. 

Historiskt intressant är också den svenska [AMIS](#amis) och
[Epsilon](#epsilon). Se dessa.

En hackerfavorit bland skärmeditorer. Den andra favoriten är klassiskt
sett vi.

## EMRM

Kort för enligt min ringa mening, en översättning av engelska
hackiskans IMHO, "in my humble opinion". Se även [I min
humlas opinion](#humla-i-min-humlas-opinion).

## enkelfnutt

Tecknet **'**.

## epsilon

1. En mycket liten mängd av något. Kommer från den matematiska
världen. "Det kostar epsilon att få en syltklick på glasstruten".

2. Namn på en bra, kommersiellt tillgänglig, EMACS-kompatibel editor
till MS-DOS.

3. Datorförening på Åsö Gymnasium.

## error 220

1. Fel som uppstår då man kopplar in elverket på datorbuss. Brukar ge
upphov till en del intressanta ljusfenomen.

2. Utrustningen fungerar inte, eftersom sladden är urdragen.

## esoterisk

Den som vet han vet...

## Ezen

1. Klassisk BBS i Stockholm. Numera nedlagd. Hette egentligen
"Ezenhemmer plastpåsar och barnuppfostringsredskap".

2. Lika klassiskt men ännu existerande parti med namnet "Ezenhemmer
plastpåsar och barnuppfostringsredskapspartiet". Introducerades i
valet 1985.

## fas

Förhållandet mellan det [fysiska dygnet](#fysiskt-dygn) och det
[logiska dygnet](#logiskt-dygn).

Exempel: "MC är i nattfas så han kommer nog in vid 18-tiden."

Byta fas på det svåra sättet: Vara vaken tills man är i fas.

Byta fas på det lätta sättet: Sova tills man är i fas.

## favoriterna

1. Ett bra program. En [CUSP](#cusp).

2. [EMT] Några program i systemet Mechslide som har ett antal
negativa kvaliteer ur programmerarsympunkt (till exempel KUGGV & KT)
bland annat för att 1. Koden är skriven av icke-EMT personal, och är
därför ofta buggfylld, svår att porta, följer ej EMT:s riktlinjer,
nyttjar ej våra funktioner med mera. 2. Ingen tänkande varelse klarar
av att testa programmet, för mängden indata som krävs är enorm,
obegriplig och odokumenterad (KUGGV). 3. Ingen någonsin använder dem.

## Fedi

Se [Fediversum](#fediversum).

## Fediversum

[Från engelska "Fediverse"] Familjenamn på flera olika sorters
decentraliserade sociala nätverk där de flesta är ihopkopplade genom
ActivityPub-protokollet. Exempel på serverprogram som används:
Mastodon, Pleroma, Misskey, Diaspora* med flera.

## fitjur

[Engelskans "feature"] En bra funktionalitet. Typiskt ny
funktionalitet som ska in i ett program eller en hårdvara. Har ibland
komplicerat förhållande till [bugg](#bugg).

## fippla

Fixa och dona lite med något. Se också [diddla](#diddla).

## fiska disk

1. Försöka förmå andra användare som använder mycket av ens delade
   diskutrymme att radera filer. En del organisationer har/hade en
   skampåle där de som använder mest disk listades så man visste vilka
   man skulle gnälla på.

2. Samla oallokerat diskutrymme, ursprungligen i samband med att man
tappat (tagit bort av mistag) sin enda kopia av ett program.

## fiskmås

Namn på tecknen "{" och "}" (vänster respektive höger fiskmås). Även
kallade måsvingar.

## fixas lätt av den händige

[CD, basvrak] Indikerar att någonting med en minimal insats kan
ordnas av en person kunnig i det berörda ämnet. "Cachade
inställningsfiler i ctwm fixas lätt av den händige."

Ibland ironiskt för något som är oerhört komplicerat, inte alls fixas
lätt, och kanske till och med är bevisat omöjligt.


## Flognat

[Lysators KOM] "Var inte en sådan Flognat!" Eller "Detta är troligen
en flognat, men vad är foo?" Efter Andy "Flognat" Eskilsson, FUKT som
var känd för att ställa frågor i KOM som lämpligen besvarades med
"RTFM!".

Vi har fått Flognats explicita tillåtelse att inkludera detta.

## flämt

Utrop vid förvåning eller förtjusning, troligen inspirerat av
[MUD](#mud). Ofta i formen "Flämt!".

## fnutt

Tecknet **"**. Ibland dock tecknet **'** lite beroende på lokal
kultur. **"** kallas då "dubbelfnutt".

## foo

Den kanoniska metasyntaktiska variabeln. Används också till
slaskvariabel, temporärfiler och allt annat.

Andra liknande uttryck är bar, baz, gurksallad, zoo, foobar, kaka,
allan, hatmax och hasmax.

"Allan tar kakan", vanlig nonsensmening från ELVIRA-miljön.

"Antag att vi har två rutiner, låt oss kalla dem foo och bar. Om nu
foo anropar bar..."

## FORTRAN

FORmal TRAsh Notation. Fult ord. Skrivs ofta "F-N". Används som
svärord eller i förolämpningar, till exempel "Din mamma skriver
ritprogram i Fortran!" som blev lite väl nära när en kvinnig hacker i
Linköping både började bygga hydrologiska modeller i Fortran på SMHI
och blev mamma.

Egentligen ett programspråk från 1950-talet, FORmula TRANslation,
ursprungligen avsett för beräkningar inom till exempel fysik. Lever
kvar framförallt i superdatorvärlden. Saknade i äldre versioner
förmåga till rekursion och överhuvudtaget strukturell
programmering. Ogillas ofta av hackers, kanske framför allt eftersom
så mycket Fortran-kod är [fysikerkod](#fysikerkod).

## frobyt

FROM BY TO (WHILE DO OD)-djur. Ett av de tre dator djuren frobyts,
buggar och gremlins.

## Fuck me harder

Brukar skrikas om datorn verkligen djävlas med en. Ibland "Fuck me
harder with a chainsaw!" för de mest extrema fallen. Nästan alltid på
engelska, av någon anledning.

## furk-bee

Så uttalas FIRQB, en minnesarea på en DEC PDP-11, där man lägger sina
parametrar när man anropar [RISTES](#ristes).

## fult

1. Kod eller hårdvarudesign som är osmaklig eller inte upprätthåller
   den standard en hacker förväntar sig. Exempelvis en ISA som inte är
   tillräckligt ortogonal.

2. (Äldre användning) Används om ett program som gör något på ett
   (inte nödvändigtvis intelligent men) oväntat eller kraftfullt sätt.
   Kallas även att [smartkoda](#smartkoda), motsats till att
   [rakkoda](#rakkoda) som tar hand om saker på enklast sett med hjälp
   av ren, rå datorkraft.

## fysikerkod

Kod skriven av en icke-hacker men inte nödvändigtvis en fysiker.
Möjligen är det kod skriven i [Fortran](#fortran) men en skicklig
Fortran-programmerare kan skriva Fortran i vilket programspråk som
helst.

## fysiskt dygn

Det vanliga 24-timmarsdygnet. Ovanligt att hackaren är i [fas](#fas)
med det.

## fånklicka

Hoppa från länk till länk utan bestämt mål på världsvida väven.

## fårtran

1. Annan populär beteckning på känt programspråk, se
   [FORTRAN](#fortran).

2. Fettmassa som utvinnes ur bräkande lurviga djur.

## förstå

1. Om program, kunna bearbeta data givna på ett visst format, precis
som en människa förstår olika språk. "- Förstår den SVG? - Nä, bara
äldre vektorformat."

2. Utrop av frustration till en oförstående data: "Förstå!"

## gama

Tigga, be om. Typiskt för att försöka få en kopia av ett program.

## gamer

1. \/gäjmer\/ Person som ägnar omåttligt mycket tid åt datorspel.

2. [Från demoscenen] Uttalas som det stavas. En [lamer](#lamer) som
ständigt går runt och tigger spel av folk ([gamar](#gama) spel).
Uttrycket är en sammanslagning av orden "game", "gam" och "lamer".

## garb

[Från Lisp]

1. Betecknade från början orefererade data, som då och då måste städas
bort ur systemet för att inte ta onödig plats. Processen kallas
"Garbage Collect" (förkortas GC). Garb används om saker och ting som
ligger och skräpar. 

"Värma upp GC", FOA-uttryck för att generera mycket garbage.

2. Namnet på klubbtidningen för datorföreningen [Lysator](#lysator).

## garba

Städa bort garb. Typiskt använt om till exempel Emacs: "Min Emacs
hänger när den garbar."

## gatväg

Skämtsam översättning av engelskans "gateway", en förbindelselänk i
ett datornät.

## gazonk

1. Gammalt standardlösenord på LiU.

2. Metavariabel i linköpingskulturen.

3. Alias för Sten-Åke Lindell, DVL 81 vid Uppsala Universitet.

Ursprunget är antagligen att defaultfilnamnet i en nystartad EMACS var
"GAZONK.DEL".

## GEB

*Gödel, Escher, Bach: Ett Evigt Gyllene Band* (*Gödel, Escher, Bach:
an Eternal Golden Braid*), Brombergs 1985, av Douglas R. Hofstadter.

En av hackerdoms [biblar](#bibel).

Boken uttrycker ett slags filosofi om att kognition och medvetande är
ett resultat av gömda processer i komplexa system som påfallande ofta
har något att göra med rekursion. Detta exempliferas med flera
formella system men också konst och musik. Den har blivit något av en
kultbok bland hackers. Rekommenderas!

## generisk

Allmän, standardmässig. "Vad skall vi äta? - Ja, det blir väl pasta
med någon generisk sås."

## Genesis

Det första [MUD:et](#mud) med [LPMUD](#lpmud) som motor. Körde på
[CD](#cd).

För att spela, se [Genesis MUD](https://www.genesismud.org/).

## gifflar

Pluralis av GIF (Graphics Interchange Format, ett populärt
bildformat). En GIF, flera GIFflar. Numera sällan använt av hackers
eftersom företaget Unisys i sin ondska beslutade att stämma alla som
använde den komprimeringsmetod som användes i GIF. Använd i stället
gärna PNG (Portable Network Graphics).

## gladman

Teckenkombinationen ":-)" eller liknande, som ser ut som ett leende
ansikte och indikerar ironi eller skämtlynne; engelskans smiley.
Observera att även :-( kallas gladman, fast den är sorgsen.

## goon symbol

Semikolon i programspråket ALGOL 68. Uttalas "go on".

## gremlin

1. Mytologiskt väsen, som förorsakade oförklarliga störningar i
elektroniken i RAFs radarsystem under andra världskriget

2. En SEMA i ALGOL 68. När man gör UP gremlins, kan det börja hända
konstiga saker, särskilt med filer. Men det är faktiskt the gremlins
som ser till att programmet fungerar ungefär som i [RR](#rr), trots
värddatorns ogästvänliga operativsystem.

3. Person, som har samma effekt på operativsystem, som en riktig
gremlin.

## grina

[CD, basvrak] Klaga, ondgöra sig, sura i största allmänhet. Uttalas
med fördel med ett rejält utsträckt 'i' och en kraftigt accentuerad
örebrodialekt. "Var är Emil? - Han sitter väl och grinar i ett hörn
någonstans."

I sin imperativform innebär ordet en uppmaning att sluta grina, så som
ordet definierats ovan. "Jag är trött, och så är jag hungrig också. -
Äh, grina!"

## gröna faran

En datorterminal av märket Facit 4431 med grön bildskärm (ungefär RGB
#36ff00). Förr mycket vanlig i [PUL](#pul) på [LiU](#liu) och senare mycket vanlig hos
[Lysator](#lysator) och hemma hos en del [lysiter](#lysit).

## gud

En systemansvarig men i synnerhet systemansvarig på ett [MUD](#mud),
vanligen ett [LPMUD](#lpmud). Överste wizard.

## hack

1. Ett snabbt jobb som fixar något, men inte mer.

2. Resultatet av 1.

3. Snyggt hack: En listig lösning.

Se till exempel Stephen Levys *Hackers: Heroes of the Computer
Revolution* för mer om betydelsen av "hack" och "hacker".

## hacka

Att programmera. "Vad gör du?" --"Jag hackar AMIS".

## hackare

Svensk variant av [hacker](#hacker).

## hacka sönder

Att göra en massa oförsiktiga ändringar så att resultatet blir
oanvändbart. Även att modifiera.

## hacker

Ordet har en lång historia och kan betyda många saker. Ibland ska det
tolkas som flera av nedanstående betydelser samtidigt.

1. En person som är extremt skicklig i något, till exempel
   programmering, hårdvarukonstruktion eller andra, ofta
   datorrelaterade, konster som inte begränsas av arbiträra regler.

2. Någon som uppskattar en särskild skönhet, till exempel snygga
   [hack](#hack) eller [hackvärde](#hackvärde).

3. Någon som identifierar sig som medlem i den världsvida
   hackerkulturen av programmerare, hårdvarudesginers och andra.

4. En person som tar sig in i datorsystem, se [cracker](#cracker)
   betydelse 2. Kontroversiell betydelse bland en del old school
   hackers.

## hackerwasser

En gång i tiden antingen Jolt Cola eller Coca Cola. Nu för tiden också
[Club-Mate](#club-mate), Flora Power och liknande yerba mate-baserade
drycker.

## hackintosh

Ursprungligen en Apple Macintosh-klon inbyggd i Apple II-låda,
exempelvis det hembygge Johan Ringström donerat till Lysator.

Nu för tiden oftare en vanilj-PC med macOS på.

## hackiska

Hackerslang. Kan också kallas hackeriska, på engelska
"hackerish". Kallas ibland för "jargongen", fast det strikt talat
inte är fråga om jargong längre, utan slang.

## Hackmatic

En modifierad terminal av typ Datamedia Elite 1521, så att
TAPE-tangenten längst ned till vänster. på tangentbordet (under Ctrl)
fungerar som Meta-tangent, det vill säga att den sätter 8:e biten i
tecken som sänds. Denna feature är mycket bra att ha då man kör bland
annat AMIS, som för övrigt av hysteriska skäl har kommandot "M-X
Hackmatic terminal" för att slå på tolkning av just Meta-kommandon.

## HAKMEM

[MIT AI Memo
239](https://hack.org/mc/writings/hackerdom/hakmem/hakmem.html)
(Februari 1972). Klassisk samling eleganta matematik- och
programmeringshack.

## hackvärde

Anledningen till att göra något till synes meningslöst, bara för att
resultatet är ett snyggt hack. Till exempel är kommandot "M-X Phase of
the moon" i AMIS eller kommandot "(Vad är) månens fas" i LysKOM:s
TTY-klient inlagt enbart för sitt hackvärde.


## hallå!

Vanligt utrop av hackers då systemet är trögt, det vill säga då
svarstiderna är långa.

## hat!

Rytande när saker skrynklar sig alldeles ovanligt mycket. Ryts vid
AIDA vid de mest oväntade situationer... Även "Grr" har förekommit.

## HB

Flippra ur, gå i ett obestämt sömntillstånd. Tappa intresset för
något. Exempelvis "Gå i HB", "Hibernera".

## hem-hemma

Hemma hos föräldrarna.

## hen

Se hän.

## historiska skäl

Vanligt uttryck för att förklara varför till exempel saker har så
konstiga namn (till exempel CAR och CDR i Lisp: Contents of Address
Register respektive Contents of Decrement Register, namn på halvord i
en gammal IBM-dator), eller varför saker beter sig så kostigt. Se även
[hysteriska skäl](#hysteriska-skäl).

## hJon

Ett nedsättande uttryck om vissa personer som förpestar folks tillvaro
inom BBSande och KOM-system. Skrivs av historiska skäl som "hJon".

## hjärndöd

Ett uttryck som används i negativ betydelse, mestadels om
program. "Det här programmet har ett hjärndött I/O-paket!" Används
vanligen inte om personer.

## humla, I min humlas opinion

Skämtsam ljudhärmande översättning av engelskans "in my humble
opinion", enligt min ödmjuka åsikt, enligt min ringa mening.

Har blivit allt mer frikopplat från originaluttrycket så att man nu
kan säga "min humla håller inte med din" eller "Min humla har ledigt
så jag har ingen åsikt" och liknande.

## hysteriska skäl

Rolig omskrivning av [historiska skäl](#historiska-skäl).

## hård boot

När maskinen har coreat så grovt att man inte ens kan [CADa](#cada)
den, då är det dags för hård boot. Detta involverar oftast att [bita
av sladden](#bita-avs-ladden), att trycka på
[ANY-tangenten](#any-tangenten).

## hårig

Onödigt invecklad, obegriplig. "Kommandona till CHANGE är otroligt
håriga!"

## hän

Även stavat "hen" eller "h?n". Han/hon. Könsneutralt pronomen,
troligen inlånat från finskan. Förekom redan på 80-talet i svenska
hackerkulturen.

## händ!

[AIDA, Uppsala] Används när det går klister i maskinen, det vill säga
när datorn är så nedlastad att man undrar om den hade dött eller
fortfarande tuffade på. Likartat med [hallå!](#hallå).

## hänga sig

Råka in i ett tillstånd där inget händer, om program och maskiner.


## IDI

[NannyMUD wizards] Idiot. Används ibland synonymt med [luser](#luser).

## imponans

Egenskapen hos saker att imponera, genom sin tekniska sofistikering,
hi-tech finish, et cetera. "Din nya Cray 2 har en otroligt hög
imponans!"

## imponanskvot

Förhållandet mellan imponans och någon annan parameter, till exempel
ålder, antal radiorör, vikt, storlek, och dylikt. E-sektionen på LTH
(Lunds Tekniska Högskola) hade en gång en lång, invecklad formel för
hur imponanskvoten för ett elektronikhembygge beräknas ur antalet
lysdioder, transistorer, integrerade kretsar, rör, motorer och övriga
rörliga delar, motstånd med mera.

## imponansvärde

Se [imponans](#imponans).

## imponatorpanel

Panel med en stor mängd lampor och knappar. Lamporna (som helst ska
vara i olika färger) ska blinka i obegripliga mönster och på något
obskyrt sätt avspegla tillståndet inuti burken. Förekommer ofta på
gamla sortens stordatorer, t.ex. IBM 360, DEC-10, UNIVAC 1108
etc. (Nutida moderna datorer är tråkiga i jämförelse, bara stora
stumma skåp.)

## indian

Antyder vilken bit i ett ord som är mest signifikant, stor indian
eller liten indian. Från "big endian" och "little endian".

## interrupt

Ett avbrott mitt i något annat, varefter man påbörjar något nytt
(tillfälligt) för att sedan återgå till orginaluppgiften. Kan till
exempel vara ett tillfälligt byte av samtalsämne. Se [CDR](#cdr) och
[poppa](#poppa).

## interrupta

Tillfälligt påkalla någons uppmärksamhet. En request för någon att
stacka.

## intro

Liten [demo](#demo). Ett program som liknar en demo men som är till
för att presentera vad nu ska presenteras.

## invändare

Den sorts individer som använder Mechslide m.m. I stort sett synonymt
med [luser](#luser), men oftast har invändare bitonen av att de har
haft mage att klaga...

## IRC

\/irrk\/ Ett decentraliserat chatsystem på Internet med anor
från 1988. Fokus på IRC är gruppchattarna, kanalerna, snarare än
1:1-kommunikation. Det är ett ganska enkelt protokoll, så därför finns
en uppsjö av servrar, klienter och [bottar](#bot).

Från början fanns ett enda IRC-nätverk som alla IRC-servrar anslöt sig
till. Med tiden splittrades det av olika orsaker till flera nät. Några
IRC-nät med hackiska associationer är till exempel:
[Libera](https://libera.chat/), [OFTC](https://www.oftc.net/) och
[hackint](https://hackint.org/).

Det går förstås att köra en IRC-server helt standalone också, utan att
vara ansluten till ett nät. Om du har hackare på din arbetsplats är
det ganska troligt att det någonstans i ert nätverk finns en liten
IRC-server som kanske bara lyssnar på localhost och är lite svår att
upptäcka.

## IRQ

\/I-ärr-ku\/ Står för Interrupt Request. Ropas av nån som vill skjuta
in nåt mitt i ett samtal som inte är det aktuella samtalsämnet. Se
[interrupt](#interrupt), [pusha](#pusha) och [poppa](#poppa).

## IÅM och I]M

Kortform för [Inlägg åt mig](#inlägg-åt-mig).

## Inlägg åt mig

[Egentligen Inl{gg }t mig] Ett möte i Lysators KOM skapat av Thomas
"Padrone" Padron-McCarthy när han hade stirrat på [Se
tiden](#se-tiden) lite för länge med en rätt egen kultur. Här får man
inte hoppa över inlägg. Alla ska läsas. Helst ska alla kommenteras på
också. Fungerar mer som en chat än som ett traditionellt KOM-möte.

Se även Hans Perssons [I]M: Verkligheten som icke-kausal textuell
diskurs](https://hack.org/mc/texts/IAM.txt).

## Jag ska bara

Vanligt utrop då en hacker blir avbruten av ett gäng andra hackers på
väg att gå och äta. Det är inte så lätt att bara avbryta mitt i ett
jättehack.

Illustreras ofta på de chattar som tillåter sådant med en custom emoji
som visar Alfons Åberg.

## joina

1. Göra JOIN i SQL på två tabeller i en relationsdatabas.

2. Det hårda arbete som krävs att fixa ihop två programs olika
[sås](#sås) som två olika programmerare ändrat i olika ändar av,
särskilt utan hjälp av moderna versionshanteringsverktyg. Se också
[merga](#merga).

## jojo-mod

Beskriver det tillstånd hos datorer, där de har en tendens att dyka
alldeles för ofta. Även jojoläge.

## Jolt

Koffeinrik hackercola. Riktiga Hackers får programmerarpris av Jolt.

## JSYS

[Ursprungligen beteckningen på ett operativsystemanrop i Twenex, Jump
to SYStem.] "Dra en JSYS": Gå och handla ut på Systembolaget, även
känt som [SYS:](#sys:).

## julfest

Typiskt vid Halloween eftersom Oct 31 = Dec 25, om man tänker lite som
en amrisk hackare.

Medlemmar i Ctrl-C firar även jul i februari av outgrundliga skäl, men
de har alltid varit lite märkliga. Se även [AFDD](#afdd).

## junk

Odefinierbar hårdvara, vanligtvis oanvändbar. Foo med avseende på
hårdvara. Se även [bojsänke](#bojsänke).

## jyckenät

En alternativ, och något nedsättande, benämning på hobbynätverket
Fidonet eller ett nätverk baserat på samma teknik. Kallas även Fight
'o net, på grund av de många stora policy-striderna.

## kaka

Vanligt skräpvariabelnamn eller filnamn. Se också [Allan](#allan).

## katten Pejst

Rolig omskrivning av cut & paste.

## kallkompis

Calcomp digitizers, ofta menande en 2500 dig.

## kanna

Integrerad krets. "Det går åt cirka 20 kannor för att bygga det där
minneskortet." Se även [pyts](#pyts) och [kapsel](#kapsel).

## kanelbulle

Benämning på tecknet **\@**.

## kanonisk

Normaltillstånd, vanligt beteende, det vanliga sättet att göra
något. "Det kanoniska sättet att knäcka ett godtyckligt program är
att slå vilt på tangentbordet i tjugo minuter". (Denna metod är även
känd på Stacken som "[Mathiasmetoden](#mathiasmetoden)").

## kapsyl

Integrerad krets, kapsel. Se även [kanna](#kanna) och [pyts](#pyts).

## kasta upp

Skicka upp [sås](#sås) eller andra filer till en server. Se även
[tanka](#tanka).

## kerwham!

Onomatopoetiskt ord ofta beskrivande en krasch.

## klingon

Obehaglig person som tror sig vara hacker men i själva verket är en
[moby](#moby) luser. Mycket irriterande sak.

Ursprungligen Fienden i TV-serien Star Trek.

## kläggboll

En liten kaka av kokosflarn och kakao, även känd som chokladboll, ofta
av typen Delicatoboll. Klassisk frukost i [PUL 17](#pul-17) eller
[Q](#q-huset): En CC och två kläggbollar.

## kläggig

Om programmering, ormbo. Vidrig kod. Värre än [hårig](#hårig).

## KM

Guden över alla Stockholms modemare. Efter en av Jan Mickelins katter:
Katten Mickelin.

Se [KMs Goda Bok](https://melin.org/kmgb/), en sammanställnig av
Tobias Hellsten över de öden, vittnesmål och profetior KM-trogna fått
uppleva under årens lopp.

Se även [Ezen](#ezen).

## knattra

Programmera, eller på annat sätt trycka på datorns tangenter. Se
också [smeka](#smeka).

## knäckare

Se [cracker](#cracker).

## KOM

Ursprungligen namnet på en fleranvändar-BBS på [QZ](#qz). Numera en
familj av BBS-program med ett lite speciellt användargränssnitt. Till
skillnad från de hatade [menykläggen](#menyklägg) så har KOM-familjens
BBS:er ett vänligt kommandoradsgränssnitt med automagisk expansion. Se
[Manual för KOM
v6](https://www.lysator.liu.se/lyskom/nostalgi/kom6gm/). Se också
[KOMpatibel](#kompatibel).

Se också Jacob Palmes [History of KOM Conference
System](https://people.dsv.su.se/~jpalme/s1/history-of-KOM.html) och
[utdrag ur QZ KOM](https://people.dsv.su.se/~jpalme/qzkom/).

## Kommandorörelse

Typiskt något komplicerat kommando.

Se [svordom](#svordom) betydelse 1.

Se även [speciell kommandorörelse](#speciell-kommandorrelse) för upphov.

## KOMpatibel

OBS stavning. Ett konferenssystem som efterliknar KOM i "look and
feel", det är KOMpatibelt med KOM. Motsatsen är ett
[menyklägg](#menyklägg).

Exempel på KOMpatibla system är till exempel MikroKOM till CP/M och
MS-DOS, [LysKOM](https://www.lysator.liu.se/lyskom/) där serverdelen
kör på [*nixar](*nix) och till skillnad från många KOMpatibla kräver
en särskilt klient, The Common Link (TCL) till MS-DOS, NiKOM till
AmigaOS och många fler.

## komplicera

Roligt omskrivning för att "kompilera".

## K&R

Kerninghan, Brian W. & Ritchie, Dennis M: *The C Programming
Language*.

## kragg

Kraftaggregat.

## krasch

Händelse att program eller utrustning slutar fungera.

## krascha

1. Åstadkomma krasch: jag råkade krascha ditt program. 

2. råka ut för krasch: programmet kraschade.

## krfsm

[CD, basvrak]

1. Ett ting (oftast) eller en företeelse. "Ge mig krfsmen på hyllan
där." Se även [böp](#böp).

2. substitut för oläslig eller oviktig text, företrädesvis
handskriven, vid högläsning. "...Med vänliga hälsningar, krfsm krfsm."

3. Allmänt uttryck för missnöje med världens hård- och kantighet.

4. Yttras det med ett frågande tonfall, kan det även betyda "vad sade
du?".

## kvadd

Se [Autokvadd](#autokvadd).

## kycklinghuvud

Benämning på Commodores logotyp som har en viss likhet med ett
kycklinghuvud: "C=".

## kärra

Se [burk](#burk).

## label

Etikett på en person, inte sällan elak sådan.

## lamer

Uttryck från demovärlden. En person som tror sig kunna allt och vara
bäst på allt men som i själva verket inte klarar av att göra
någonting. Väldigt ofta används ordet av personer som själva är det.
jämför [klingon](#klingon).

## lappis

Bärbar dator. "Har du några bra klistermärken till min nya lappis?"

## leet

Slang för "elite", bland de bästa, bland somliga [crackers](#cracker).
Används ofta ironiskt.

## leetspeak

Ett ovanligt korkat sätt att skriva text på genom att byta ut
alfabetiska tecken mot siffror, t ex genom att skriva "1337" istället
för "leet", som i sin tur är ett slangord för "elite".

Används ofta ironiskt bland hackers och crackers.

## lepra

Det "femte hjulet". När någon inte får plats i gemenskapen till
exempel femte man vid fyrmansbordet på en resturang.

## lina

1. Benämning på en telefonledning, jämför [snöre](#snöre).

2. Nätverksförbindelse, ofta något längre än lokala Ethernetkabeln.

## Lisp

1. En familj av programspråk. Många hackares favorit.
   [LANG-LIST](https://hack.org/mc/texts/lang-list.txt) säger:

> LISP - LISt Processing. John McCarthy et al, MIT late 50's. Symbolic
> functional recursive language based on lambda-calculus, used
> especially for AI and symbolic math. Many dialects. Atoms and lists.
> Dynamic scope. Both programs and data are represented as list
> structures.

2. Förkortning för Lots of Infernal Stupid Parentheses.

## LLL

Förkortning för Lage, Lave och Laila, Lysators tre första Sun-datorer
(Sun 3/50). Trots att Lysator med tiden blev överhopat med Sun-datorer
kvarstod LLL som en beteckning för Lysators Sun-nätverk även fast de
tre ursprungliga maskinerna sedan länge gått i pension: "Funkar
gcc-47.11 på LLL?"

## LPC

Lars Pensjös C efter hackaren Lars Pensjö, [CD](#cd). Ett programspråk
inne i [MUD-spelet](#mud) [LPMUD](#lpmud) som används för att bygga
världen och alla objekt i den, inklusive spelare.

Också inspiration till programspråket µLPC som senare utvecklades till
[Pike](#pike).

## LPMUD

Hackaren Lars Pensjös [MUD](#mud), ursprungligen från 1989. Extremt
inflytelserikt i Sverige där det kördes flera stora instanser,
däribland [Genesis](#genesis) och [NannyMUD](#nannymud) men också för
att det inspirerade så många till att lära sig programmera i
[LPC](#lpc) och senare i andra programspråk.

Det fina med LPMUD är att när du når wizard-nivån blir du själv
programmerare och kan göra vad som helst inne i spelet genom att
skriva [LPC](#lpc)-kod för att beskriva eller ändra alla objekt,
inklusive spelarna. För att hålla wizards någorlunda i schack, så att
de till exempel inte introducerar kärnvapen i en fantasyvärld, finns
särskilt utsedda Arch Wizards och, förstås, [gud](#gud).

## luring

Tankefälla.

## lus

Samma sak som en bugg. (Nära ordagrann översättning.)

## luser

Användare. Uttalas med svenskt "u".

## LYS-16

Beteckning på 16-bit datorbyggsats framtagen av datorföreningen
[Lysator](#lysator). Datorn är konstruerad med "bit-slice" teknik.

## lysfika

Lysators kaféverksamhet.

## lysit

Medlem i datorföreningen [Lysator](#lysator).

## lyssoffan

[Lysator] Även "lussoffan". Soffa längst in på
[sandfiltret](#sandfiltret). Favoritplats för somliga hackers,
vanligen med en [grön fara](#gröna-faran) nedvinklad från bordet och
tangentbordet i knät.

Sent på [logisk natt](#logisk-natt) sov ibland någon på den. Var den
upptagen fanns en Cray 1, som ju ser ut som en lobbysoffa, att sova på
en trappa upp. Den senare är dock hemskt obekväm.

## logisk dag

När dygnsrytmen säger att du ska vara vaken. Har ingenting med solen
att göra.

## logiskt dygn

Det dygn hackaren är i [fas](#fas) med.

Exempel är 28-timmarsdygnet i en vecka på 6 logiska dygn. 8 timmars
sömn ger då 20 vakentimmar och gott om tid att bli klar med det där
enorma programmeringsprojektet. Praktiskt också eftersom du efter en
vecka är i fas med resten av världen igen.

## logisk natt

När dygnsrytmen säger att du ska sova. Har ingenting med solen att
göra.

## läcka

Egenskap hos ett program som allokerar resurser (till exempel minne)
men inte återlämnar dem efter användning. Detta leder till att
resursen så småningom tar slut, varvid programmet kraschar.

## mabovändning

Att byta åsikt utan att erkänna eller ens låtsas om att man har gjort
det. Antagligen uppkallat efter Roland Mabo.

## mackan

Annat namn på Apple Macintosh. Kallas även "Mac:en", "Hackintosh". "En
Mac", liktydigt med en Macintosh.

## MAD

1. Förkortning för den gamla datorföreningen Malmö Amatör Datorister.

2. Förkortning för MötesADministratör, något som förekommer i vissa
KOMpatibla system.

## magi

Något komplicerat, svårförklarat. Vanligt förekommande i
hackervärlden, som är starkt beroende av magi.

## magiskt

Något obegripligt eller som tar en timme att förklara och därför
viftas bort. "Det där är magiskt (så det orkar jag inte förklara
nu)".

## main lupe

Klassisk felstavning av "Main Loop". Fanns i alla högklassiga
systemprogram på bland annat ELVIRA och används fortfarande ibland i
kod.

## makron

Se [makrougnen](#makrougnen).

## makrougnen

En Philips industriell mikrovågsugn av tidig modell som fanns i
Lysators tidiga lokal PUL17, inköpt billigt på försvarets
överskottslager. Det krävdes fyra personer för att lyfta upp den på
det kylskåp där den stod så den förtjänade inte prefixet "mikro-" även
om den inuti var den lika liten som modernare ugnar. 

Det gick inte att ansluta makrougnen till samma fas som datorer i [PUL
17](#pul-17), för den sänkte spänningen när den gick igång.

Flera [gröna faror](#gröna-faran) som stod i närheten av makron när
den gick igång fick nippran, blinkade med skärmarna och leddarna och
var allmänt omöjligt att använda. Det gick också bra att värma
händerna genom att hålla upp dem framför luckan medan ugnen var igång.
Det är ett under att lysiterna som var i närheten fortfarande är vid
liv.

## manul

Annat ord för manual, det vill säga handbok.

## manöverpult

Konsoll. Ibland sagt om huvudsakliga skärm och tangentbord för något.
Ibland terminal kopplad till seriellporten som en [*nix](#nix) anser
är console.

## massera

Behandla data, filtrera.

## Mathiasmetoden

Effektiv metod för felsökning av program. Tillgår så att man lägger
handflatorna på tangentbordet och sedan hamrar vilt i 20 minuter. Om
programmet fortfarade lever, är det idiotsäkert. (Uppkallat efter
Mathias Båge, den enda hacker som kan knappra i bolero-takt på ett
tangentbord). Kallas även för [armbågstestet](#armbågstestet),
eftersom det kan utföras genom att lägga armbågen på tangentbordet.

## -max

Tillägg till utrop för att understryka, till exempel [hatmax](#hat)
eller [zonkmax](#zonkad).

## med en farlig fart

[Ferdinand Fitzschklopfs *Uppror!*] När något utförs raskt, typiskt
när data [masseras](#massera).

## menyklägg

Benämning på icke-KOMpatibelt BBS-system, vanligtvis utan
kommandoradsgränssnitt och istället en serie av tvingande menyer som
tar en massa tid att skriva ut i låga modemhastigheter. Typiskt också
oftare inriktade på fildelning snarare än diskussioner.

## merga

Fläta ihop olika grenar i ett versionshanteringsverktyg.

## Messy-DOS

Microsofts MS-DOS. Även kallat MES-DOS.

## meta

1. Ursprungligen ett tecken med 8:e biten satt, ofta skapad genom att
   trycka på Meta-tangenten samtidigt som annan tangent.

2. Knapp som används ofta i likhet med Ctrl-tecken som kommando till
   olika program. Kan ofta emuleras i terminalfönster med ett tryck på
   ESC-knappen om Meta inte fungerar.

Se också [hackmatic](#hackmatic).

## mikroprofessor

Rolig omskrivning av "mikroprocessor". Se också
[propeller](#propeller).

## MIPS

1. Million Instructions Per Second. 

2. Förkortning för "Meningslös Information från Påstridiga Säljare".

## misär

Beklaga (eventuellt i ironisk bemärkelse). Exempelvis: "Det är misär
när diskpacken krashar."

## mit schnelle

Utrop för att understryka brådska. "Jag behöver den nya versionen av
operativsystemet mit schnelle!!!"

## mjäk

Metauttryck som används främst vid [NADJA](#nadja). Betyder inte.

## mjäkvara

1. Mikrokod.

2. Firmware ("styvvara").

## moby

1. Stor, enorm eller komplex.

2. En adressrymd (256 K) på en PDP-10.

3. Tilltalsord för att visa respekt eller beundran. "Temple of the
   Moby Hack".

## modemare

Någon som ringer BBS:er med hjälp av uppringd förbindelse och modem.

## modermodem

Från hysteriskt rolig tidningsartikel där det stod "Modermodemet,
själva hjärtat av hårddisken".

1. En dators moderkort.

2. Något centralt i ett komplext system som kontrollerar andra saker,
   till exempel Active Directory i ett nätverk med Windows-datorer.

## mojäng

Medelstor sak, vilken som helst, med knappar, rattar eller annat som
kan manipuleras.

## morsabräda

En dators moderkort.

## mosa

Tryck, ös, drag, se även [pusha](#pusha).


## MUD

Multi-User Dungeon eller möjligen Multi-user Dimension beroende på hur
allvarlig du vill vara. En familj av textäventyrsspel för flera
samtidiga användare.

Ursprunget är det MUD som kördes på en PDP-10-dator på University of
Essex i Storbritannien (MUD-1). MUD-1 skrevs av Roy Trubshaw och
Richard Bartle med början 1978. Detta ur-MUD kördes också på somliga
DEC-10:or i Sverige.

MUD var inspiration till en stor familj av liknande spel. En av de
mest inflytelserika är [LPMUD](#lpmud) där du när du "vinner" spelet
blir magiker och kan börja programmera saker inne i världen med
programspråket [LPC](#lpc). Ett stort fortfarande körande LPMUD är
[NannyMUD](#nannymud) på [Lysator](#lysator).

[SvenskMUD](#svenskmud) var ett annat LPMUD på Lysator.

BSX-MUX var ett experiment med en 2D-grafisk variant av LPMUD
nederländska hackern Bram Stolk körde på Lysator. Krävde till skillnad
från ordinarie LPMUD en speciell klient.

Andra liknande system som är mindre inriktade på spel och är mer
inriktade på det sociala är familjerna MUSH (ibland "Multi-User Shared
Hallucination" eller kanske "Multi-User Shared Hack, Habitat eller
Holodeck"), MUCK och MOO. I de här varianterna kan vanligen alla
användare, inte bara magiker, expandera världen inifrån.

Ett exempel på en MUCK i svensk hackerdom är [Sociopolitical
ramifications](#sociopolitical-ramifications).

## månens fas

En slumpmässig parameter, som man inte kan utpeka. Exempel: "Detta fel
uppstår bara udda torsdagar vid fullmåne". "Om det där fungerar, beror
det på att man har FOO-switchen satt, de senaste tio kommandona och på
månens fas."

## måsvinge

Se [fiskmås](#fiskmås).

## märkligheten

Rolig omskrivning för [verkligheten](#verkligheten) som ännu mer
distansierar talaren.

## mösslås

Rolig översättning av "Caps Lock".

## N

Ett godtyckligt (stort) tal. "Det finns n buggar i det där
BASIC-programmet!" "För n:te och sista gången: ..."

## nalle-klappa

Mycket långsökt quadrupel-omskrivning av muspekare. Muspekare ->
Pusmekare -> Puh-Smekare -> Nalle-Klappa.

## NannyMUD

Det största MUD-spel som drivs av datorföreningen Lysator. Namnet
kommer av att det ursprungligen fanns på datorn [nanny](#nanny). Före
den tiden hette det BrutalMUD och ObelMUD, efter respektive datorer.

[NannyMUD:s vävplats](https://www.lysator.liu.se/nanny/).

För att spela: `telnet mud.lysator.liu.se 2000`.

## nere

Stillastående, ej i drift.

## nil

[Lisp] Nej, inte, tomt, ingenting, falskt. Motsats: "t". Vanligt
svar på frågor enligt P-konventionen. "Jag fattar nil av den här
håriga manulen!"

## NIL Lisp

Martin "NIL" Nilssons Implementation of Lisp (på ELVIRA), av elaka
tungor kallat "a Non-Intelligent Lisp".

## *nix

Någon slags Unix, till exempel Linux, [*BSD](#bsd), Solaris, HP-UX,
True64, et cetera.

## njutoptimerat

Beskrivning av något (vanligtvis program) som är bra. Uttalas oftast
med inlevelse: "njiiuuutoptimerat!"

## njuth

Nåt man gör när det fungerar.

## normalstig

Default path, connected directory.

## no-op

Betecknar något som inte gör något. Härstammar från
maskininstruktioner som brukar kallas "NOP" och som bara hoppar fram
till nästa instruktion. "Den här tomma Coca-Cola burken är en no-op!"

## NUCCC

Nordic University Computer Clubs' Conference. En årligen återkommande
träff av akademiska datorföreningar i mitten av juni.

## NÄDK

Se [När är du klar?](#när-är-du-klar)

## När är du klar?

Vanligen ironiskt menad fråga när någon kommit med ett kul, men
icke-trivialt, hackförslag. Förkortas ibland i skrift till "NÄDK?".

## nära livet-upplevelse

En upplevelse som, om än bara för ett kort ögonblick, ger en försmak
av känslan av att ha ett liv. "Jag hade en nära livet-upplevelse idag.
Jag var och tittade på en ny lägenhet."

## Nätskräp

Nedsättande eller bara skämtsam beteckning på WWW-klienten
Netscape. Ibland även Netrape.

## nätvärk

1. Illamående på grund av illa fungerande datanät.

2. Illa fungerande datanät.

3. Skämtsam stavning av nätverk.

## o-

Prefix som skapar motsatskonstruktioner. Används ofta i stället för
vanliga motsatser för att poängtera att det inte är ordet i sig utan
själva motsatsförhållandet som är det viktiga i sammanhanget. Till
exempel kan frågan "Vad skall vi äta idag?" besvaras med "Olever!",
vilket då markerar att personen som svarar kan tänka sig att äta
precis vad som helst utom lever. Den oftast använda konstruktionen är
ordet obra, men o-prefixet kan användas i alla sammanhang man önskar.
Konstruktionen torde härröra från rikets nordligare delar.

## obra

Dåligt men antagligen mycket, mycket dåligt. Det mest använda exemplet
på användning av o-prefix. "Det vore obra om nån hade en generell
kvantdator."

### ogräs

[Från MUD] Det engelska ordet "ogre" har ingen entydig svensk
översättning. Det heter ömsom stygg trollkarl, jätte eller troll. En
mer ljudenlig översättning som förekommer vid plural och vid tal om
kollektiv är ogräs.

## op

[IRC](#irc) channel operator. Någon som har [bittar](#bittar) på en
[IRC](#irc)-kanal. Nämns vanligen av någon [klingon](#klingon) som
hoppar in på kanalen och säger "Op era horungar!".

Även i verbform "oppa": Ge någon op över en kanal.

Du kan ibland få op av en [bot](#bot).

## oxe

Se [dialogoxe](#dialogoxe).

## -P

[Lisp] P-konventionen gör ett predikat med sanningsvärde av något.
Besvaras med t (ja) eller nil (nej). Exempel:

"MatP?" betyder "Ska du med och äta?".

"HemmaP?" betyder "Sitter du och kör hemifrån?".

## pecemist

Persondator kompatibel med IBM PC.

## pessimal

Skämtsam motsats till optimal.

## pekare

Vill man veta var något ligger eller hur man kontaktar dom ber man om
en pekare dit, till exempel:. "Har du en pekare till ett företag som
säljer sibbar?". Not för icke-programmerare: En pekare är något som
innehåller adressen till något.

## permafrost

Lustiga fenomen som uppstår på en bildskärm (speciellt Elite
terminaler), vid elektrostatiska urladdningar.

## peronist

[AIDA] Anhängare av peronism, en vid vissa datorsystem utbredd
ideologi som är hämmande för hackers. Kan även stavas päronism. Har
inget med frukt eller Argentina att göra.

Ursprungligen från Uppsala under 1970-talet kring AIDA. Ett tag hette
systemadministratörerna Per och Per, alltså kollektivt "Peronen".
Peronism var det dessa ansågs syssla med.

## Pike

Programspråk som utvecklades av [lysiter](#lysit) med inspiration från
[LPC](#lpc) och deras tid spelande [MUD](#mud)-spelet [LPMUD](#lpmud).

## pilla bittar

Syssla med programmering på mycket låg nivå, en aktivitet som brukar
resultera i att man sitter och räknar ut hur varenda bit i programmet
används.

## pizzascroll

En metod för att scrolla grafik på skärmen med bitplanpekarna. Hette
ursprungligen "Rönninge pizzakartongmetoden" Kommer från ett [copy
party](#copy-party) i Rönninge där metoden förklarades med skisser
ritade på en begagnad pizzakartong.

## ping

1. Ett program och protokoll som frågar "Är du där?".

2. En person som gör samma sak, i betydelsen "höra av sig", "få tag
på", till exempel "Jag pingade dig i onsdags men ingen data kom
tillbaka."

3. Uttryck för tillstånd av lycka och/eller välbefinnande. På frågan
"Hur är läget?" kan man alltså svara "Ping!". Motsatsen till denna
form av ping är [blargh](#blargh). Självklart kan dessa båda ord böjas
efter tycke och smak: "Jag kände mig pingad men alla andra satt bara
och blarghade, så jag gick hem.".

## poka

[Från BASIC:s POKE] Stoppa in något i minnet.

## pong

Svar på frågan "Ping?" (Är du där? - Jag är här.).

## poppa

Av stackoperationen POP. Återgå till en tidigare aktivitet eller
samtalsämne. Motsats: [pusha](#pusha).

## poppa upp

Att visa (ta fram på skärmen) en dialogbox.

## porjus

[LUDD] Enhet för effekt. Något som drar mycket ström. Från
kraftverket i Porjus (480,6 MW). "- Hur mycket drar den där VAX
8650:n? - Ett par porjus."

## Professionella Programmerare

[CTRL-C, Lysator] Skrivs ofta med stora begynnelsebokstäver. Används
ironiskt för sådana som får betalt för att göra ett undermåligt jobb.

En anonym Ctrl-C:are förklarade för nollorna under en presentation av
de båda datorföreningarna vid LiU att Ctrl-C minsann körde program
skrivna av Professionella Programmerare med hänvisning till
konferensprogramvaran NOTES i jämförelse med Lysators LysKOM. Den
stackars C-c:aren blev senare mycket tråkad för detta.

Om du tror att det Lotus Notes som avses ovan behöver du skaffa dig en
[Riktig Dator](#riktig-dator).

## programätaren

Ett litet djur som kryper omkring och avlägsnar alla program
(speciellt källkod), som inte används på länge eller sparats på
arkivmedium. Programätarens färg är grön. Den lilla gröna
programätaren har varit framme igen. Under Twenex finns det en
"officiell" liten grön programätare, som kallar sig Reaper. Den sparar
programmen på magnetband, som det tar dagar att få tillbaka från.

## programröta

Hypotetisk sjukdom som drabbar program som sällan eller aldrig
används, och därför blir inkompatibla med nyare versioner av
systemprogramvara. Kallas även "bitsönderfall", (bit decay). Se även
[bitröta](#bitröta).

## propeller

Processor. Ibland cores på en processor. "Hur många propellrar har den där?"

## pug

En trevlig eller nyttig bug. Kallas också [fitjur](#fitjur).

## PUL

[LiU] Terminalsal, av programutvecklingslaboratorium. Vid
Institutionen för datavetenskap (IDA) och datorcentralen (LIDAC,
numera UNIT) fanns flera numrerade PUL.

## PUL 17

PUL 17 var namnet på Lysators ökända källarlokal som användes
1987-1992. Den låg i källaren vid skyddsrummen under ingång 29 i
B-huset på Valla campus. Se även [PUL](#pul) och
[sandfiltret](#sandfiltret).

## pullie

En rullgardinsmeny: "Man kanske skulle smeka in KT på en pullie?"

## pult

1. Enkhram Pult, en halvorchisk stridsman i D&D-kampanjen Femur i
   Uppsala. Enkhram Pult ansåg sig vara bäst, störst och vackrast.
   Enkhram Pult var "Pult, Guden!", helt enkelt. Enkhram Pult sköttes
   av ORC. "Femur" betyder för övrigt "lårben" på latin.

2. Pult Guden, lokal gud vid AIDA.

3. Beteckning på person med stor makt över ett datorsystem. Se
   [wizard](#wizard).

Se också [pulta](#pulta) och [manöverpult](#manöverpult).

## pulta

1. Aktivera sina privilegier.

2. Av misstag göra något katastrofalt.

3. Göra något som bara en pult kan, till exempel flytta filer till
   någon systemarea.

## pusha

1. Av PUSH (Stackoperation). Att avbryta något (till exempel ett
  samtalsämne) för att senare återgå till det. "Kan du inte pusha det
  där och följa med och äta?" Motsats: [poppa](#poppa).

2. Drag, ös: "Är det nån pusha i en [Sol](#sol)?"

## pusmekare

Från Puh-smekare. Skämtsam omskrivning av muspekare. Se också
[nalle-klappa](#nalle-klappa).

## pyts

1. Dator. Se också [burk](#burk).

2. Integrerad krets.

## Pyttemjuk

Skämtsam översättning av Microsoft.

## på rot

Genast. Även uttrycket "på rotmos" förekommer.

## pää-zää

Hatbenämning på PC på den tiden det inte ansågs vara en [Riktig
Dator](#riktig-dator). Kan fortfarande gälla en PC med Windows.

## pölen

Atlanten, exempelvis "ftp:a en gig över pölen" eller "Vi på högra
sidan om pölen".

## Q

Se [Q-huset](#q-huset).

## Q-huset

Lysators lokal 1992-2000(?). En barack som hade använts som
pastorsexpedition som Lysator, Ctrl-C, Admittansen och några till
köpte för 1 kr.

Döptes efter Hans Alfredssons *Varför är det som ont om Q?*.

## raka en yak

En rekursiv förberedelse för att egentligen göra något annat. Exempel:
"För att få in den här fitjurn så måste vi först raka en yak".

## rakkoda

Att programmera på enklast möjliga sätt genom att använda rå
datorkraft. Motsatsen till [smartkoda](#smartkoda), se även
[fult](#fult). "When in doubt, use brute force", som Ken Thompson sa.

## random

Slumpmässig.

## randompoka

Trycka in data slumpmässigt i minnet.

## rekursion

Se [rekursion](#rekursion).

## rekommenderas av fackman

Åtgärd som rekommenderas av fackman (hacker).

## ribba

Nåt man får när programmet fungerar: "Fy sjutton vilken ribba! KT
susar!"

## Riktig Dator

En dator en hackare hackar på till skillnad från vad folk brukar ha
hemma. Förr i tiden oftast en större dator, till exempel en VAX,
PDP-11 eller DEC-20. Senare antagligen en arbetstation, till exmpel
från Apollo, Sun eller HP eller för all del någon av de åtråvärda
Lispmaskinerna. Idag troligare PC-hårdvara med en [*nix](#nix) på.

## RISTES

Operativsystem till minidatorn PDP-11. Det heter egentligen RSTS/E,
men det är så svårt att säga.

## RKM

"Amiga ROM Kernal Manual". En [bibel](#bibel) för programmerare av
Commodore Amiga.

## rogerfälla

Tankefälla, som uppkommer vid försök att täppa till lucka, och som 1.
inte täpper till denna helt, eller 2. öppnar nya möjligheter för en
[cracker](#cracker).

Uppkallat efter en före detta systemansvarig på ELVIRA.

## RR

Vanligt namn på *Revised Report on the Algorithmic Language ALGOL 68*.

## RTFM

Read The Friendly/Fucking Manual. Används när man berättar om hur
någon klantat till det för att hän inte visste hur ett program
fungerade, eller ens brydde sig om att ta reda på det.

## rycka

Används ofta i samband med [snöre](#snöre). "Rycka i snöret" innebär
att sända logiska signaler på ledningen, vanligtvis logisk etta.

## Rydnet eller Rydnät

Ett projekt för att installera ett datornätverk i
studentbostadsområdet Ryd vid [LiU](#liu). Under hösten 1992 började
projektet förverkligas efter stora ansträngningar av bland andra
Magnus Redin och Kjell "kjell-e" Enblom. Kjell-e loggar i oktober
samma år för första gången in på en dator, Lars Aronssons liv, över
Rydnet från sin egen dator, [tekla](#tekla).

## rymdkrog

Mellanslagstangent, skämtsam översättning av engelskans "space bar".

## rymdretur

Trycka mellanslag och sedan retur (förekom i KOM).

## rå

Något som inte är avsett för människor. "Skicka mail med rå SMTP".

## råtta

Mus. Även "rådis": "Smeker man med råttan så susar det men smeker
man med diggen så blir det coffee wood av alla skrålplankor!"

## s///

(Från ed/ex/vi) Korrigering av något tidigare skrivet i en chat.
Exempel:

```
<foo> barf
<foo> s/bar/foo/
```

Betyder att foo skrev fel och egentligen ville skriva "foof".

## sandlådan

Se [sandfiltret](#sandfiltret).

## sandfiltret

En del av Lysators lokal [PUL 17](#pul-17) som var byggd ovanpå ett
luftfilter till ett skyddsrum. Höjden mellan golv och tak var cirka
120 centimeter men det var inte lågt i tak utan högt i golv. Utrymmet
var inrett med soffa, stolar, bord och terminaler.

Det höga golvet gjorde också att lysrörsarmaturen i taket hade blodiga
hörn.

## SASYNE

Mnemo för syntax error på RSTS/E, identiskt med "så synd!" Se
[så synd](#så-synd).

## SBS

Förkortning för "Skit Bakom Spakarna". Avser att ett fel inte beror på
systemet, utan på användaren.

## sexigt

Ett program som är snyggt, vackert, genomarbetat, snabbt,
användarvänligt eller liknande. Gärna med extra finesser.

## sexiga detaljer

Se [snuskiga detaljer](#snuskiga-detaljer).

## SH

Shit Happens. Bra uttryck för oförklarliga fenomen.

## SICP

[sick-pee] Hal Abelson, Gerald Sussman och Julie Sussman: *Structure
and Interpretation of Computer Programs*. Också känd som "Wizard
Book".

## sjutton-spel

När du försöker få in fler saker än vad det finns plats för. Vanligen
använt om att försöka få in 17 kubikmeter gamla datorer i ett 16
kubikmeter stort lager. Ordet kommer från Datorföreningen Stacken, som
är experter på den sortens spel.

## självraderande

Har ingenting med att radera filer att göra. En mycket mycket långsökt
omskrivning av "självklart" (med omväg via engelskan): Självklart ->
Self-clear -> Självraderande.

## skedar

Nivån på mental energi, ofta för en person med neuropsykiatriskt
tillstånd, som är ganska vanligt i hackerkulturen. "Jag har inte
tillräckligt med skedar för att hantera det där just nu." Från Ego
Depletion Theory och Christine Miserandinos "The Spoon Theory".

När du får slut på skedar har du nått [zonkmax](#zonkad).

## skrika

Att skriva något med enbart stora bokstäver.

## skrynk

Gå sönder på ett spektakulärt sätt, framförallt mjukvara. Varianter
som "skrynk!", "skrynkla (sig)".

## skrynkel

Händelsen att papper fastnar i en skrivare eller kopiator. I överförd
betydelse att något knasat sig. Se också [skrynk](#skrynk).

## skrålbräda

[EMT] Scrollbar. De där små tingestarna i kanten man rycker i med
råttan. Ibland "skrålplanka".

## skrålplanka

Se [skrålbräda](#skrålbräda).

## skräptecken

Om man sitter uppkopplad via modem över en dålig förbindelse kommer
vissa störningar att visas på skärmen i form av oönskade tecken,
så kallade skräptecken.

## skärmorgasm

Degaussande av en klassisk CRT-skärm.

## slask

1. Tecknet **/**.

2. Något tillfälligt, exempelvis en slaskvariabel eller slaskfil.

## slippa

Förlora ett program, inte få eller ha möjlighet att göra, bli av med
något värdefullt. Till exempel slippa köra (ett eventuellt
priviligierat) program som har fel skyddskod. Speciellt vanligt som
"Du slipper!" som svar när folk frågar hur de ska kunna göra nåt de
vill göra.

## slem

[AIDA] Nedsättande benämning på det mesta. Exempel "Slem!", "slemmig".

## slurj

Uttryck för något rysligt, oordnat, rörigt, blandat, entropiskt.
"slurj" är den svenska översättningen av "gobble". Ordet förekommer i
Philip K. Dicks bok "Martian Time Slide". Bokens hjälte ser alla
rubriker som vanligt, men all annan text är ersatt med slurj, slurj
slurj. Slurj slurj? Slurj! "Nej, jag orkar inte hacka KOM idag, jag
var på partaj igår och känner mig rätt slurjig." "-Jag hörde att det
finns 17 nya kommandon i JCL." "-Slurj!"

## smak

Variant, typ, sort. "Kommandon till AMIS kommer i två olika smaker:
Ctrl-kommandon och Meta-kommandon". Jämför [vanilj](#vanilj).

## smartkoda

Att med hjälp av alla fula knep göra sitt program exempelvis litet,
kort, eller snabbt (tror man!) Se även [fult](#fult) och
[rakkoda](#rakkoda).

## smeka

1. Trycka på nåt med musen: "Smek ner den där rullgardinsmenyn.".

2. Jobba med datorn: "Vad gör du?" "Jag smeker burk.".

3. Lägga till en ändring: "Här får du filen FOOFIX.TXT, smek in den i
XYZ.C och testa.".

## smutt och smuttigt

Något som är tjusigt, trevligt eller allmänt tilltalande: "Gud vilken
smuttig funktion!" eller "Den där VAX:en var smutt". Det senare kan
också tänkas implicera litenhet.

## snarfa

Att ta något.

## snuskiga detaljer

Synnerligen teknisk information. "Ge oss alla snuskiga detaljer om
den nya Alpha-processorn!"

## snöre

Benämning på elektrisk förbindning, ledning, oftast något man kan
överföra data i, som till exempel seriesnöre.

## SO

[Från amrisk hackiska] Speciellt Omtyckt{e/a}, översättning av
Significant Other. Könsneutral hänvisning till en eller flera personer
i talarens eller skrivarens omgivning. Kan mycket väl vara äkta man
eller hustru. SO kan också referera till flera personer, antingen i
relationer av polyamoröst snitt eller till andra personer i ens
familj, till exempel barn, men det senare är sällsynt.

Ibland skall förkortningen, berönde på kontext, uttolkas som
SexObjekt.

## Sociopolitical ramifications

[Sociopolitical ramifications](https://sprmuck.org/) är ett
chatorienterat [MUD](#mud) med furryinriktning, tidigare på
[Ctrl-C](#ctrl-c). Kör MUCK, programmeringsbar inifrån med Multi-User
Forth (MUF).

## socker

Något som visserligen är onödigt, men väldigt trevligt att
ha. Exempel: Expansion av kommandon i Twenex eller moderna unixskal.

## sol

1. Dator av fabrikat Sun Microsystems. Inte den faktiska tidigare
   hemdatorn Sol.

2. Himlakropp som hackers sällan ser.

## spark

Dator med SPARC-processor. Vanligen från Sun men även från andra tillverkare.

## sparkstötting

Se [spark](#spark).

## Speciell kommandorörelse

1. Göra något komplicerat.

2. Göra något väldigt enkelt men som för en utomstående kan se
komplicerat ut.

Kommer från att Tommy Ekström, VD för Voice Integrated Nordics i en
intervju i DN fällde de bevingade ordenn "de som kan sådant här kunde
göra något slags speciell kommandorörelse och slinka in bakvägen" för
att förklara varför deras server med massor av inspelade 1177-samtal
låg helt öppet på Internet.

## Spela MB

Att spela dum. Kommer antagligen från den ökände Marcel Bos (MB), en
[klingon](#klingon) från Södertälje. MB har också identifierats med
Roland Mabo.

## spec

Specifikation, normativ dokumentation, uttalas spes.

## sprak

Sun SPARCstation. Se [sol](#sol) eller [spark](#spark).

## sprattla

Som [rycka](#rycka) men i avseende på att ta emot signaler. "Nu
sprattlar det äntligen i snöret!"

## späka

Ösa, skyffla, t.ex späka disk, späka control-C.

## stacka

Tillfälligt byta samtalsämne, samtalspartner, eller liknande, för att
senare återuppta detta. Går att applicera rekursivt. Se [pusha](#pusha)
och [poppa](#poppa).

## stafettpinne

Svenk benämning på "token". Ett begrepp som används inom nätverk där
en stafettpinne (token) skickas runt för att signalera vem som kan
sända t.ex. IBMs TokenRing (ett stafettpinnenät).

## stenbrottet

CD:s utgrävning under en hörsal för att få mer lokaler. Takhöjden
varierade mellan fyra meter och 0.5 meter.

## Stugan

Textäventyrsspel som hackarna Kimmo Eriksson, Viggo Kann, Olle E
Johansson började skriva i BASIC när de var 10, 12 och 14 år gamla
1977-78 på [ODEN](#oden) på [QZ](#qz). Senare blev Stugan omarbetad
och utgiven för MS-DOS.

Mötet Thorvalds Stugråd i [KOM](#kom) var till för diskussioner om
Stugan.

[Hela källkoden](http://kimmoeriksson.se/stugan/STUGA.txt) till
Stugan.

## störta

Samma sak som krascha.

## sumpa

Förstöra eller tappa bort något. "[IDI](#idi)! Sumpade du just databasen!?"

## SuperTOPS

Av svenska hackers vansinnigt hackad version av TOPS-10 med både
TCP/IP och många andra förändringar.

## susa

Program som (kanske oväntat) fungerar, ofta i samband med portning:
"Susar editorn?" "Jadå, allt susar utom KT!" eller: "Nämen! Nu
susar det!"

## supa bort

Slarva bort, radera av misstag, eller överkopiera med fel version av
ett program eller en [sås](#sås): "Vem fan är det som har supit bort
min fix i KT?"

## svart hål

1. Något där saker bara försvinner. "Mitt brev studsade inte, utan
försvann bara i ett svart hål."

2. Specifikt om en terminallinje som inte är kopplad någonstans.
Uppträder vanligtvis i terminalväxlar, genom att man begär kontakt med
en viss dator, och får en icke-existerande linje.

## SvenskMUD

Linus Tolke översatte 1991 [LPMUD](#lpmud) till svenska och skapade en
spelvärld efter svenska förutsättningar. SvenskMUD öppnade för
allmänheten 29:e juli 1991. Detta var troligen världens första MUD på
svenska.

## svorak

Svensk variant av tangentbordlayouten Dvorak. Typiskt är apostrof,
punkt och kommatecken i övre vänstra delen av layouten utbytta mot Å,
Ä och Ö. Det finns flera varianter till exempel A1, A5 och
[Blowrak](#blowrak).

## svordom

1. En kommandorad, typiskt ganska lång, eller argument för ett
   kommando som skiljer sig från de normala, t ex `dd`:

   `dd if=/dev/random of=~/foo bs=1M size=1024`

2. En regexp.

## svårt

[CD, basvrak] Mycket, väldigt. "CM5 är en svårt häftig maskin."

## swapper

1. [demoscenen] En person som sysslar med att kopiera och skicka
disketter till andra swappers. Kallas ibland för mail swapper för
tydlighetens skull. I och med den ökande användningen av modem har
swappern i princip blivit utkonkurrerad av [tradern](#trader).

2. En process som har som uppgift att slänga ut saker från minnet till
disk på [Riktiga Datorer](#riktiga-datorer).

## swascii

Någon av de svenska versionerna av amerikanska 7-bitars teckenkoden
ASCII, ISO 646-SE eller ISO-646-SE2, igenkännbar framför allt för att
de har svenska tecken:

ISO 646-SE: }{| ][\\ (åäö ÅÄÖ).

ISO 646-SE2, utökad för namn: }{|`~ ][\\@^ (åäöéü ÅÄÖÉÜ).

## syntaktiskt socker

En språklig konstruktion som man egentligen inte behöver, men som gör
koden enkel, lättläst etc. "Syntactical sugar causes cancer of the
semicolon."

## SYS:

Skämtsam kortform för Systembolaget, som i enheten under äldre OS.

## så synd

Ironiskt beklaga, till exempel: Så synd, du slipper (återfå ditt
program). Se också [SASYNE](#sasyne).

## sås

Försvenskad benämning på källkod, "source". Kallas även såskod och
liknande.

## såsboll

Ett arkiv med källkod, oftast i formen av gzippad eller bzippad tar,
foo.tar.gz eller foo.tar.bz.

## sänka maskinen

En person som kör något ovanligt resurskrävande program kan få höra
det ovänliga tillropet: "Måste du sänka maskinen?" Används även då
maskinen [dyker](#dyka) pga av någon operativsystembugg. "Jag sänkte
maskinen när jag gjorde remove på disken i stället för dismount."

## t

[Lisp] Ja, sant, rätt. Motsats till [nil](#nil).

## tarboll

Ett filarkiv, vanligen men inte alltid, i tar-format. Typiskt också
gzippad eller bzippad. Se också [såsboll](#såsbol).

## tanka

Överföra en fil. Används i "tanka ner" för att överföra fil till sig
och "tanka upp" för att överföra från sig. Exempel: "Jag tankar ner
din version istället."

## tdm

Förkortning för "total depravation of money". Legendarisk
hackerförening från Nynäshamn. Drev även en legendarisk BBS ett
flertal år med samma namn.

Ska alltid skrivas med små bokstäver.

## teknologsäker

Sak som tål köttyxa. Funktionssäker burk, omöjlig att lura. Klarar
till och med [mathiasmetoden](#mathiasmetoden). (Det går att göra
saker 1) Idiotsäkra, 2) Barnsäkra, men det går *inte* att göra dem
teknologsäkra.)

## telekräkningen

Lustig omskrivning av "teleräkningen" som tydligt visar vilka känslor
en ivrig [modemare](#modemare) får när hän får densamma.

## terminera

Avsluta, kan gälla teckensträngar som avslutas med nolltecken, kablar
som avslutas med ett motstånd eller aktiviteter.

## THHGTTG

Adams, Douglas: *The Hitch-Hiker's Guide to the Galaxy*.

## tilt

Används för att beskriva något som har stoppat eller eljest är
dåligt. Ordet har sitt ursprung från s.k. flipperspel, där slag eller
stötar på maskinen avbryter pågående spel och tänder en skylt "TILT"
på panelen. Får man även när man slår och bankar på terminalburken för
att få datorn att gå fortare.

## tilt error

Helt oväntat och/eller obegripligt fel, gärna med ett felmeddelande
som inte säger någonting om vad som egentligen är orsaken. Ofta
orsakat av att användaren har gjort något som får helt andra effekter
än hän tänkt sig. Se [tilt](#tilt).

## titt som då

Titt och tätt blandat med då och då.

## toota

1. Att skriva ett inlägg i [Fediversum](#fediversum), framför allt genom
en Mastodon-instans.

2. Att starta om sin dator, barnsligt uttal av boota. Se också [bita
av sladden](#bita-av-sladden) eller [hård boot](#hård-boot).

## toss

[Stacken, Ctrl-C] Inget att ha. Ofta gjort med medföljande gest av att
slänga något över axeln. Skrivs vanligen som \*toss\*.

## trader

Uttryck från demovärlden. Kallas även "modem trader" för
tydlighetens skull. En person som sysslar med att ringa olika baser
och tanka (huvudsakligen) piratkopior, kallade [warez](#warez).

## trappa

Fånga, ofta om en interrupt.

## trasrutt

[Lysator] Rolig omskrivning av "traceroute" ett program för att
upptäcka just trasiga nätverksruttar.

## trendriktigt

Nåt som är hemskt bra, eller eventuellt hemskt dåligt, till
exempel: "KT är ett trendriktigt program!".

## Träsket

AutoDesk. Ursprungliga formen är [Autoträsk](#autoträsk).

## tröska

1. Av engelskans "thrash", när en maskin är så belastad att den bara
swappar i stället för att arbeta.

2. Bearbeta (stora mängder) data.

## tröskverk

Gammal, seg dator, till exempel en Apollo Domain eller en PDP-11/23.

## tschunk

En drink gjord på Club-mate (eller liknande, som Flora Power, 1337
Mate, ...). Bra när man vill nå [Ballmer peak](#ballmer-peak).

- 4-6 cl rom, gärna mörk.
- En limefrukt.
- Club-Mate.
- Lite brunt socker.
- Krossad is.

Skär limefrukten i skrivor. Lägg tillsammans med brunt socker i ett
highballglas. Krossa. Lägg i isen. Häll i rommen och Club-mate.

Kan också serveras som slush ur en slushmaskin, som till exempel
NOC:en på [C3](#c3) gör.

Variant: Använd Bourbon istället för rom och du får en "American
Hacker".

## tuggpekare

Rolig försvenskning av engelskans "byte pointer".

## tur

Fenomen som mondäna ofta förlitar sig på. Riktiga Hackers behöver
ingen tur, eftersom deras program tveklöst går igenom samtliga
kompileringar utan fel. Eller hur?

## tvättmaskinsprocessor

Allmän beteckning på en 8-bitars mikroprofessor, en sån där som man
bruka sätta i tvättmaskiner, brödrostar, JAS och dylika tingestar.

## tweaka

Vricka till nåt så det fungerar med en ny version av nåt annat: "Jag
har ändrat foo-funktionen i KT, du får tweaka om all sås som kallar
den!".

## TWENEX

Skämtsam benämning på operativsystemet TOPS-20 från DEC för deras
PDP-10:or.

TOPS-20 kommer från TENEX (TEN EXecutive från BBN) som utvecklades på
ombyggda PDP-10. TOPS-20 hade minnesmappade filer, en jobborienterad
processrymd, ett filsystem med generationer och många bittar per fil
och katalog och ett seperat användarskal med inbyggd expansion av
kommandon.

## TWENIX

En 1:a-Aprilhack av [pultarna](#pult) på på AIDA i Uppsala (ett
TWENEX-system) som fick operativsystemet att se ut som och i viss mån
bete sig som Unix (ett helt annat operativsystem).

## uppe

I drift, i rörelse.

## usaiansk

Att ha att göra med USA. Se också [amrisk](#amrisk).

## utomjording

[Lysators KOM, ålderdomligt] Kvinna. Sågs då sällan på samma planet
som en [hacker](#hacker). Sedan dess har viss utjämning skett. Jämför
[älg](#älg) och älj som beskriver faktiska utomjordingar.

## V

Förkortning av "version". Till exempel "Jag hörde att det har
kommit en 8-bitars V av AMIS".

## vanilj

Något vanligt. Något som är standard, ej förbättrat av någon
hacker. Ointressant. Se också [smak](#smak).

## verkligheten

1. Plats där programmering nämns i samma andetag som Java, et cetera.

2. Plats där programmerare inte förekommer eller där man inte sysslar
   med programmering.

3. Utanför universitetsvärlden.

## VFSH

Vad eller Vem Fan Som Helst, kan användas som variabelnamn.

## VIMS

Skämtsamt namn för för DEC:s (numera en del av HP) operativsystem VMS,
numera känt som OpenVMS. Även [VUMS](#vums).

## voida

Kasta bort ett uträknat värde.

## vrål

Mycket (förstärkningsuttryck) t.ex. VRÅL.DAT = stor slaskfil,
speciellt i samband med att man [fiskar disk](#fiska-disk).

## VUMS

Se [VIMS](#vims).

## välja att

Multifunktionell konstruktion ämnad att understryka viss
irrationalitet eller slumpmässighet hos en handling eller en händelse.
Nyttjas framför allt vid beskrivning av händelseförlopp och
framställande av skrönor. Typiska användningsexempel är "Då valde
servern att gå ner" och "Jag valde att spilla skållhet glögg på min
hand".

## vän, x är din vän

[Lysators KOM] Vanligt, och relativt vänligt, utrop till någon som
söker information eller en funktion, där X till exempel kan vara lika
med ett program, en sökmaskin, en manul eller källkoden för ett
program: "Duckduckgo är din vän!"

## warez

[Demoscenen] Betyder (oftast) piratkopierade spel. Hackers skämtar
gärna med detta och de som är ute efter warez.

## wizard

Se [överstepräst](#överstepräst).

## wheel

1. [Överstepräst](#överstepräst) på [TWENEX](#twenex). Efter "WHEEL
CAPABILITY" en bit i TWENEX-användares capability word som ger en
möjlighet att göra allt.

2. Medlem i gruppen wheel på [*nixar](#nix). Och/eller någon som kan
   göra `doas` eller `sudo`.

## yanka

Plocka in nåt i något annat, ofta menande [sås](#sås): "Yanka in den
där raden mellan rad 125 och 126", ofta när man skall [merga](#merga)
två såser. Men det har även en ganska allmän betydelse: "Yanka in den
där boken i hyllan".

## Zdm

Kortform av [Z'ha'dum](#zhadum)

## Z'ha'dum

[Från Lysators KOM, ursprungligen TV-serien Babylon 5] Stockholm,
Sveriges huvudstad. Flitigt använt i fraserna "If you go to Za'ha'dum,
you will die", ibland förkortat "IYGTZ, YWD" eller "Z'ha'dum, a dark
and terrible place". Ibland förkortat som Zdm.

## zonkad

Mentalt trött, psykiskt utmattad. "Du ser ju helt zonkad ut! - Ja, jag
har labbat halvledarteknik i åtta timmar."

## å

Så kan höger hakparentes "]" eller fiskmås "}" uttalas. Se
[swascii](#swascii).

## ä

Så kan vänster hakparentes "[" eller fiskmås "{" uttalas. Se [swascii](#swascii).

## älg

1. En Sun Sparcstation ELC. ELC har även uttytts som Extra Low Cost
eller Extremely Low Cost på grund av den klena processorn.

2. (LSFF, CTRL-C, Lysator) Alien. Utomjordingar som inte
(nödvändigtvis) är kvinnor. Stavas även "älj".

## älj

Alien. Se [älg](#älg) betydelse 2.

## Älgönät

Svensk benämning på internetleverantören AlgoNet, numera en del av
Telenordia.

## äpplet

Annan beteckning på Apple II.

## ÄVKY

[Från Lysators KOM, ursprungligen Äpple, vanilj, kanel-yoghurt]. Det
högsta goda. Något som är oerhört bra.

## ö

Så kan [bakslask](#bakslask) "\\" eller vertikalt streck "|" uttalas.
Se [swascii](#swascii).

## örma

[MUD] Egentligen "\\rm". Radera ordentligt. Generaliserat från
MUD-betydelsen, där kommandot rm (remove) i LPMUD tar bort en fil. När
detta kommando förbjöds eller förhindrades, kunde filer fortfarande
tas bort genom att sätta en bakslask (som motsvarar stora Ö i
[swascii](#swascii) före kommandot.

## överstepräst

En hacker som är systemprogrammerare och har makt över ett större
datorsystem.

## Appendix 1 Signifikanta datornamn

### AIDA

DECSYSTEM-20 på Institutionen för ADB, Uppsala Universitet. Körde två
[KOM](#kom): det mer seriösa AIDAKOM och det mer hackeriska HACKOM.

Finns körande i emulerat skick i form av
[TINA](http://tina.update.uu.se/), TINA Is Not AIDA.

### CECILIA

En Datasaab D-23 FCPU hos Lysator. Ett stort arbete att skriva ett
operativsystem baserat på Forth vidtogs av Inge Wallin och Paul
Svensson (PUL-S). Forth-systemets inre tolk kördes helt i
mikrokodsminnet.

### ELVIRA

[Elektros](#elektro) PDP-11/45.

Körs fortfarande [i emulerat skick](http://elvira.stacken.kth.se/).

### KATIA

[Stackens](#stacken) DECsystem-10 (KA-10). Finns nu på [Living
Computers](https://livingcomputers.org/) i Seattle.

### KICKI

SMP-baserat DECsystem-10 (KI-10). Två KI-processorer, KIKI. Get it?
Stod i CCCC. Plåtbergs egen leksak.

Finns nu på [Living Computers](https://livingcomputers.org/) i
Seattle.

### LINUS

[LiU:s](#liu) DECSYSTEM-20. Körde [KOM](#kom) v6, LINUSKOM.

### NADJA

[NADAs](#nada) DEC-2020.

### nanny

Sequent Balance 8000 på Lysator med 16 parallella NS32032-processorer,
körandes DYNIX. Inkluderad här för att den gett namn åt
[NannyMUD](#nannymud).

### ODEN

DECsystem-10 på Stockholms datacentral [QZ](#qz). Det ursprungliga
[KOM](#kom), QZ:s KOM, utvecklades och kördes här.

### tekla

En av de första maskinerna på [Rydnet](#rydnet). Ägdes av kjell-e.

## Appendix 2 Uttryck i KOM

I ursprungliga [KOM](#kom) och dess senare efterföljare dök några
specifika uttryck upp som gällde just KOM-systemen i sig.

### geometriskt inlägg

En text som i ett ickeproportionellt teckensnitt och utan onödiga
ordmellanrum antar en geometrisk form, se även
[tegelsten](#tegelsten).

### inlägg

En text skriven med ett visst möte som mottagare.

### ingejubileum

Ett inlägg vars nummer är delbart med hundra, efter Inge Wallin, som
fick det första ingejubileumet.

### jubileum

Ett inlägg som råkar få ett jämnt inläggsnummer, för någon definition
på jämnt.

### kombatant

En debattör i ett KOM.

### komma

Att köra programmet KOM eller något derivat.

### kompis

Person som brukar köra KOM.

### KOM-trollet

Mytisk varelse som skapar oreda i ett KOM.

### möte

Konferens, samlingsplats för diskussion i ett visst ämne i ett KOM.

### något annat

Undvikande svar på frågan "Vad vill du göra?". Fanns som kommando i
KOM v6.

### palindrom

Ett inlägg vars nummer också kan läsas likadant baklänges.

### se tiden

Mycket tråkigt tecken på att man läst samtliga inlägg i KOM.

### slå om alla flaggor

Önskvärt kommando för den som kommit in i ett nytt KOM i
[torgny-mod](#torgny-mod).

### tegelsten

Geometriskt inlägg med rektangulär form.

### torgny-mod

Även "torgnyläge". Alla flaggor och inställningar står fel och gör
därmed användarna intresserade av hur man gör för att slippa dessa
misfeatures och konstiga fenomen som KOM V5 och KOM V6 har för sig när
de sitter i fel läge. Uttrycket "Torgny-mod" är uppkallad efter den
ursprunglige KOM-författaren, Torgny Tholerus, och är vanligt
förekommande uttryck bland hackers. Flaggorna var satta som
KOM-författaren tyckte att de skulle vara.

## Appendix 3 Hackiska organisationer i Sverige

### Admittansen

Elektronikföreningen [Admittansen](https://admittansen.se/) vid
[LiU](#liu). Delade ett tag [Q-huset](#q-huset) med Lysator och
Ctrl-C.

### Astrakan

Datorförening i Sandviken vid det som då hette Högskolan
Gävle-Sandviken.

### Chalmers tekniska högskola

[Chalmers tekniska högskola](https://www.chalmers.se/).

### Ctrl-C

Ursprunglingen maskinteknologernas dataförening vid [LiU](#liu).
Senare mer fokus på VMS. Ger ut tidningen Markören.

### DASH

Datorförening vid Högskolan i Halmstad. Finns inte mer.

### EMT

Ett CAD-företag där en egen hackerkultur utvecklades.

### Epsilon

Datorförening på Åsö gymnasium, Stockholm.

### ETA

[E-Sektionens Teletekniska Avdelning](https://eta.chalmers.se/).
Elektronikklubb vid [Chalmers](#chalmers). Håller årligen [en
fantastisk auktion](https://eta.chalmers.se/auction/).

### FUKT

Datorförening vid Högskolan Karlskrona-Ronneby.

### Forskningsavdelningen

Hackerspace i Malmö, 2008-2014, från början med lokaler i det
alternativa aktivitetshuset Utkanten, senare hos Kontrapunkts
verkstäder.

Höll under flera år den årliga konferensen Hacknight, som var en slags
lightversion av [C3](#c3).

### CD

Chalmers Datorförening i Göteborg. Gav ut tidningen CD-Nytt. Finns
inte mer.

### DF

Datorföreningen [DF](https://dflund.se/) vid Lunds universitet och
tekniska högskola.

### Elektro

Institutionen för elektroteknik på [KTH](#kth), Stockholm.

### ISY

Officiell förkortning för Institutionen för SYstemteknik på
[LiU](#liu). ISY hade, och har, en hel del att göra med att
datorföreningen Lysator finns. Många gamla lysiter har hamnat på ISY.
ISY är av någon anledning traditionellt mer lysatorvänliga än IDA
(datavetenskap) i Linköping.

### KTH

[Kungliga tekniska högskolan](https://www.kth.se/), Stockholm.

### LiU

[Linköpings universitet](https://www.liu.se/).

### LSFF

Linköpings Science Fiction-förening.

### Lysator

Datorföreningen [Lysators](http://www.lysator.liu.se/) vid [LiU](#liu)
grundad 1973. Systerorganisation till [Stacken](#stacken). Namnet
kommer från LYS, som tidigare var beteckning för Linköpings
Y-teknologers Sällskap. Y är linjen för teknisk fysik.

Kör [LysKOM, Lysators konferensystem](https://www.lysator.liu.se/lyskom).

### MOSIG

[Malmö Open Source Interest Group](https://hack.org/mc/mosig.html). En
lös grupp FLOSS-intresserade personer, ofta hackare, i Malmö med
tillhörande brevlista och chatkanal. Träffades länge varannan onsdag
på La Trattoria, senare Nya Tröls för att käka pizza, dricka öl och
nörda ut.

Drev under flera år konferensen Malmö Hack-a-ton.

### LUDD

Datorförening [LUDD](https://www.ludd.ltu.se/) vid [Luleå tekniska
universitet](https://www.ltu.se/).

### Medio

Datorförening vid Högskolan i Östersund.

### NADA

Institutionen för Numerisk Analys och DAtalogi på KTH.

### SAFA

Svenska arbetsgruppen för algoritmforskning. SAFA är en ideell
förening för kreativt hackande, verksam i Stockholm och Uppsala. Hade
länge tisdagsluncher med det roliga namnet Svenska Hackademin.

### SICS

Swedish Institute of Computer Science, ett institut för grundforskning
inom alla områden av datavetenskapen.

Numera en del av [RISE](https://www.ri.se/).

### Solace

Datorförening vid Mitthögskolan i Sundsvall.

### Stacken

Datorföreningen [Stacken](https://www.stacken.kth.se/) på KTH,
grundad 1978. Systerorganisation till [Lysator](#lysator).

### Acc

Datorföreningen [Acc](https://www.acc.umu.se/) vid [Umeå
universitet](https://www.umu.se/).

Tidigare TDSF.

### UFH

Från början Unga forskare Haninge. Datorförening i Haninge som hade en
egen barack som lokal, körde en DEC PDP-11/45 med [RISTES](#ristes)
med X.25-anslutning och körde UFH-[KOM](#kom).

### Update

Datorförening [Update](https://update.uu.se/) vid [Uppsala
Universitet](https://www.uu.se/).

### QZ

Stockholms Datamaskincentral för högre utbildning och forskning.
Gemensam datorcentral för FOA, Stockholms universitet och KTH. Hade
flera PDP-10:or, bland annat Oden, som körde [KOM](#kom).

Namnet QZ:s ursprung är höljt i dunkel men i hackerdom uttyds det
förstås "Qualität und Zuverlässigkeit", kvalitet och tillförlitlighet.

### Åsö gymnasium

Gymnasieskola i Stockholm som var tidigt ute med att låta elever
använda en minidator (DEC PDP-11, senare VAX) och hade tidigt tillgång
till Internet.

Körde [UFH:s](#ufh) [KOM](#kom) på sin -11, långt senare utbytt till
ett [menyklägg](#menyklägg) med fokus på [tankning](#tanka).

Idag ett vuxengymnasium.

<!--
Local Variables:
time-stamp-pattern: "8/Filen senast ändrad: [\"<]%:y-%02m-%02d %02H:%02M:%02S[\">]"
End:
-->
