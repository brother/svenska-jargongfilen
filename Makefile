TARGETS = jargonswe.html

all: $(TARGETS)

.SUFFIXES: .html .md

.md.html:
	gm -c  style.css $<

clean:
	rm $(TARGETS)
